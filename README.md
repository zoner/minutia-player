Minutia Player
==============

The Minutia Player is a terminal module music player.

Key features:

* uses libopenmpt for module playback.
* ncurses front-end that adapts to window sizes.
* look and feel based on Inertia Player by Stefan Danes and Ramon van Gorkum.

-------
AUTHORS
-------

    * Nic Soudée (zoner/xylem)

------------
DEPENDENCIES
------------

    * ncurses
    * libopenmpt
    * pulseaudio
    * portaudio
    * fftw3

------------
INSTALLATION
------------

    $ autoreconf --install
    $ ./configure
    $ make
    $ sudo make install

---------
CHANGELOG
---------

    * 1.0 Initial Release (will it ever happen?)

