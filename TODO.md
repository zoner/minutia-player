TO DO
=====

Version 1.0
-----------

* degrade completely to small player when Y size smaller than something
* degrade to one liner
* VU: scroll up/down, blue pointer on the left to indicate location
* PV: scroll left/right
* PV: bugfix pattern view, maybe rewrite for speed (ncurses window?)

Future
------

* options pane
* parametrize colors
* configuration files for colorschemes
* configuration file for colors, start pane, sound options
* playlist(s)
* spectrum analyzer

Maybe Never
-----------

* play all formats ffmpeg handles
* EQ visualization pane
* database of song metadata

