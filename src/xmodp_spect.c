#include <math.h>
#include <complex.h>
#include <fftw3.h>
#include <stdlib.h>
#include <stdbool.h>
#include "xmodp_spect.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327
#endif

void reset_spect(xmodp_spectrum_analyzer_t *a)
{
    a->ready = false;
    a->in_filled = 0;
}

void init_spect(xmodp_spectrum_analyzer_t *a, size_t buf_sz, size_t stream_sz)
{
    size_t i;

    a->in = (double *)fftw_malloc(sizeof(double) * (buf_sz + (stream_sz * 2)));
    a->out = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * buf_sz);
    a->hanning = (double *)fftw_malloc(sizeof(double) * buf_sz);
    a->result = (double *)fftw_malloc(sizeof(double) * (buf_sz / 2 + 1));
    a->p = fftw_plan_dft_r2c_1d(buf_sz, a->in, a->out, FFTW_ESTIMATE);
    a->in_sz = buf_sz;

    for (i = 0; i < buf_sz; i++) {
        a->hanning[i] = ((1 - cos(i * 2 * M_PI / (buf_sz - 1))) / 2);
    }
    reset_spect(a);
}

void cleanup_spect(xmodp_spectrum_analyzer_t *a)
{
    fftw_free(a->in);
    fftw_free(a->result);
    fftw_free(a->hanning);
    fftw_destroy_plan(a->p);
    fftw_free(a->out);
}

size_t fill_spect_buffer(
        xmodp_spectrum_analyzer_t *a, size_t buf_sz, float *buf)
{
    size_t i;
    size_t fill_amount;
    size_t rest;

    if (a->in_sz < a->in_filled + buf_sz) {
        fill_amount = buf_sz - a->in_filled;
    }
    else {
        fill_amount = buf_sz;
    }
    rest = buf_sz - fill_amount;
    for(i = 0; i < fill_amount; i++) {
        int32_t val;

        val = buf[i];
        a->in[i + a->in_filled] = (double)val * a->hanning[i + a->in_filled];
    }
    a->in_filled += fill_amount;
    if (a->in_filled >= a->in_sz) {
        a->ready = true;
    }
    return rest;
}

void compute_spect(xmodp_spectrum_analyzer_t *a, size_t buf_sz, float *buf)
{
    size_t i;
    //double phase;
    //double last_phase;

    #if 0
    size_t rest;
    #endif

    //rest = fill_spect_buffer(a, buf_sz, buf);
    #if 1
    for (i = 0; i < buf_sz; i++) {
        int32_t val;

        val = buf[i];
        a->in[i] = (double)val * a->hanning[i];
        //a->in[i] = 10 * sin((2 * M_PI)/2048*5*i);
        //a->in[i] += 20 * sin((2 * M_PI)/2048*13*i);
    }
    #endif
    if (1 || a->ready) {
        fftw_execute(a->p);
        //last_phase = 0;
        for (i = 0; i < buf_sz / 2; i++) {
            double re;
            double im;
            double power;
            double new_result;

            re = creal(a->out[i]);
            im = cimag(a->out[i]);

            power = pow(re, 2) + pow(im, 2);

            //new_result = abs(cimag(a->out[i]) / 2048);
            new_result = sqrt(power) / (buf_sz / 2) / 5;
            #if 1
            if (new_result > a->result[i]) {
                a->result[i] = new_result;
            }
            else {
                if (a->result[i] > 0) {
                    a->result[i] -= 0.5;
                }
            }
            #else
                a->result[i] = new_result;
            #endif
        }
        #if 0
        reset_spect(a);
        if (rest) {
            fill_spect_buffer(a, rest, &buf[buf_sz - rest]);
        }
        #endif
    }
}

