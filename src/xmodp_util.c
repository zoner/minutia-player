#include <math.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include "xmodp_util.h"

int ftoi(int left, int right, int prec)
{
    int prec_max = pow(10, prec);

    if (right >= prec_max) {
        right = prec_max - 1;
    }
    return left * prec_max + right;
}

char *itofs(char *s, size_t sz, int val, int prec)
{
    char str[64];
    int prec_max;

    prec_max = pow(10, prec);
    snprintf(str, 63, "%%i.%%0%ii", prec);
    snprintf(s, sz - 1, str, val / prec_max, val % prec_max);
    return s;
}

int stoi(char *s, int prec)
{
    char *ts;
    char *dot;
    char *rs;
    int l;
    int r;
    int prec_max;

    prec_max = pow(10, prec);
    ts = (char *)malloc(strlen(s) + 1);
    memcpy(ts, s, strlen(s) + 1);
    dot = strchr(ts, '.');
    rs = NULL;
    r = 0;
    if (dot != NULL) {
        *dot = '\0';
        rs = dot + 1;
        if (strlen(rs) > prec) {
            rs[prec] = '\0';
        }
    }
    l = strtol(ts, NULL, 0);
    if (rs != NULL) {
        r = strtol(rs, NULL, 0);
    }
    free(ts);
    return (l * prec_max + r);
}

char *copy_str(char *orig, size_t len)
{
    char *s;

    s = (char *)malloc(sizeof(char) * (len + 1));
    strncpy(s, orig, len);
    s[len] = '\0';
    return s;
}

void render_buf(
        float *stereo,
        float *mono,
        float *left,
        float *right,
        size_t num)
{
    int i;

    for (i = 0; i < num; i++) {
        stereo[i * 2] = left[i];
        stereo[i * 2 + 1] = right[i];
        mono[i] = (abs(left[i]) + abs(right[i])) / 2;
    }
}

int set_timer(int which, long repeat)
{
    struct itimerval itimer;

    /* initialize initial delay */

    itimer.it_value.tv_sec = 0;
    itimer.it_value.tv_usec = repeat * 1000;

    /* initialize repeat interval */

    itimer.it_interval.tv_sec = 0;
    itimer.it_interval.tv_usec = repeat * 1000;

    return setitimer(which, &itimer, NULL);
}

