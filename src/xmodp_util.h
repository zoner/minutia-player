#ifndef XMODP_UTIL_H
#define XMODP_UTIL_H

#include <stdlib.h>
#include "xmodp_const.h"

typedef struct {
    float left[XMODP_BUFFERSIZE];
    float right[XMODP_BUFFERSIZE];
    float stereo[XMODP_BUFFERSIZE * 2];
    float mono[XMODP_BUFFERSIZE];
    size_t filled_amount;
} xmodp_buffer;

int ftoi(int left, int right, int prec);
char *itofs(char *s, size_t sz, int val, int prec);
int stoi(char *s, int prec);
char *copy_str(char *orig, size_t len);
void render_buf(
        float *stereo, float *mono, float *left, float *right, size_t num);
int set_timer(int which, long repeat);

#endif

