#ifndef XMODP_SCREEN_H
#define XMODP_SCREEN_H

typedef struct {
    bool initialized;
    bool bw; /* black and white */
    int sy; /* screen width */
    int sx; /* screen height */
    int main_pos_y; /* y coordinate of top left of main outline */
    int main_pos_x; /* x coordinate of top left of main outline */
    int main_width;
    bool main_need_refresh;
    bool show_title;
    bool mini_mode;
    bool show_meta;
} xmodp_screen;

typedef struct {
    char fg;
    char bg;
    bool bold;
} xmodp_color_t;

#define XMODP_DECLARE_COLOR(a, b, c, d) XMODP_COL_##a,
typedef enum {
    XMODP_COL_NONE=-1,
    #include "xmodp_colors.h"
    XMODP_COL_COUNT
} xmodp_colors;
#undef XMODP_DECLARE_COLOR

void xmodp_set_color(xmodp_colors color_idx);
void init_video(xmodp_screen *s);
void cleanup_video(xmodp_screen *s);
bool check_screen_size(xmodp_screen *scr, bool update);
void screen_size_change(xmodp_screen *scr, int y, int x);

#endif

