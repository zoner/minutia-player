#include <stdlib.h>
#include <argp.h>
#include "xmodp_globals.h"
#include "xmodp_arg.h"

#define DEBUG_ARGS (0)
#define PRINT_ARGS (0)

static char doc[] = "\n" TITLE_HEADER;

static char args_doc[] = "FILE...";

static struct argp_option options[] = {
    {
        "fadeout",
        XMODP_ARG_FADEOUT,
        "0",
        OPTION_ARG_OPTIONAL,
        "Fade out at end of song"
    },
    {
        "tempo-factor",
        XMODP_ARG_TEMPO_FACTOR,
        "1.0",
        OPTION_ARG_OPTIONAL,
        "Tempo factor"
    },
    {
        "pitch-factor",
        XMODP_ARG_PITCH_FACTOR,
        "1.0",
        OPTION_ARG_OPTIONAL,
        "Pitch factor"
    },
    {
        "amiga",
        XMODP_ARG_AMIGA_EMU,
        "1",
        OPTION_ARG_OPTIONAL,
        "Emulate Amiga \"Paula\" chip"
    },
    {
        "oplvol-factor",
        XMODP_ARG_OPLVOL_FACTOR,
        "1.0",
        OPTION_ARG_OPTIONAL,
        "OPL volume factor"
    },
    {
        "loop-mode",
        XMODP_ARG_LOOP_MODE,
        "0",
        OPTION_ARG_OPTIONAL,
        "Loop mode (0=none, -1=inf)"
    },
    {
        "unicode",
        XMODP_ARG_UNICODE,
        "1",
        OPTION_ARG_OPTIONAL,
        "Use unicode characters"
    },
    {
        "stereo-vu",
        XMODP_ARG_STEREO_VU,
        "1",
        OPTION_ARG_OPTIONAL,
        "Stereo VU"
    },
    {
        "separation",
        XMODP_ARG_STEREO_SEP,
        "50",
        OPTION_ARG_OPTIONAL,
        "Stereo separation (0, 50, 100)"
    },
    {
        "volume-ramping",
        XMODP_ARG_VOL_RAMPING,
        "-1",
        OPTION_ARG_OPTIONAL,
        "Volume ramping amount (0-10, -1=default)"
    },
    {
        "filter",
        XMODP_ARG_FILTER,
        "default",
        OPTION_ARG_OPTIONAL,
        "Interpolation filter (default, none, linear, cubic, windowed)"
    },
    {
        "view",
        XMODP_ARG_VIEW,
        "vu",
        OPTION_ARG_OPTIONAL,
        "Default view (vu, tracks, samples, instruments, message, help)"
    },
    {
        "refresh-rate",
        XMODP_ARG_REFRESH_RATE,
        STR(REFRESH_MILLISECONDS),
        OPTION_ARG_OPTIONAL,
        "Display refresh rate (ms)"
    },
    {
        "audio-engine",
        XMODP_ARG_AUDIO_ENGINE,
        XMODP_DEFAULT_AUDIO_ENGINE,
        OPTION_ARG_OPTIONAL,
        "Audio engine (pulse, portaudio)"
    },
    {
        "samplerate",
        XMODP_ARG_SAMPLERATE,
        STR(XMODP_DEFAULT_SAMPLERATE),
        OPTION_ARG_OPTIONAL,
        "Sample rate (in Hz)"
    },
    {
        "version",
        XMODP_ARG_VERSION,
        0,
        OPTION_ARG_OPTIONAL,
        "Display version information"
    },
    { 0 }
};

static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
    arguments *arguments = state->input;
    unsigned int ui;
    int si;

    switch (key) {
        case XMODP_ARG_FADEOUT:
            if (arg == NULL) break;
            arguments->fadeout = (arg[0] == '0') ? false : true;
            break;
        case XMODP_ARG_TEMPO_FACTOR:
            if (arg == NULL) break;
            ui = stoi(arg, XMODP_CTL_PREC);
            if (ui >= ftoi(0, 0, XMODP_CTL_PREC) &&
                    ui <= ftoi(10, 0, XMODP_CTL_PREC)) {
                arguments->tempo_factor = ui;
            }
            break;
        case XMODP_ARG_PITCH_FACTOR:
            if (arg == NULL) break;
            ui = stoi(arg, XMODP_CTL_PREC);
            if (ui >= ftoi(0, 0, XMODP_CTL_PREC) &&
                    ui <= ftoi(10, 0, XMODP_CTL_PREC)) {
                arguments->pitch_factor = ui;
            }
            break;
        case XMODP_ARG_AMIGA_EMU:
            if (arg == NULL) break;
            arguments->emulate_amiga = (arg[0] == '0') ? false : true;
            break;
        case XMODP_ARG_OPLVOL_FACTOR:
            ui = stoi(arg, XMODP_CTL_PREC);
            if (ui >= ftoi(0, 0, XMODP_CTL_PREC) &&
                    ui <= ftoi(10, 0, XMODP_CTL_PREC)) {
                arguments->oplvol_factor = ui;
            }
            break;
        case XMODP_ARG_LOOP_MODE:
            if (arg == NULL) break;
            si = strtol(arg, NULL, 0);
            if (si < -1) {
                si = -1;
            }
            arguments->loop_mode = si;
            break;
        case XMODP_ARG_UNICODE:
            if (arg == NULL) break;
            arguments->use_unicode = (arg[0] == '0') ? false : true;
            break;
        case XMODP_ARG_STEREO_VU:
            if (arg == NULL) break;
            arguments->vu_stereo = (arg[0] == '0') ? false : true;
            break;
        case XMODP_ARG_STEREO_SEP:
            if (arg == NULL) break;
            ui = strtol(arg, NULL, 0);
            switch (ui) {
                case 0:
                case 50:
                case 100:
                    arguments->stereo_separation_percent = ui;
                    break;
                default:
                    break;
            }
            break;
        case XMODP_ARG_VOL_RAMPING:
            if (arg == NULL) break;
            si = strtol(arg, NULL, 0);
            if (si >= -1 && si <= 10) {
                arguments->volume_ramping_strength = si;
            }
            break;
        case XMODP_ARG_FILTER:
            if (arg == NULL) break;
            switch (arg[0]) {
                case 'd':
                    si = 0;
                    break;
                case 'n':
                    si = 1;
                    break;
                case 'l':
                    si = 2;
                    break;
                case 'c':
                    si = 4;
                    break;
                case 'w':
                    si = 8;
                    break;
                default:
                    si = -1;
                    break;
            }
            if (si >= 0) {
                arguments->interpolation_filter_length = si;
            }
            break;
        case XMODP_ARG_VIEW:
            if (arg == NULL) break;
            switch (arg[0]) {
                case 'v':
                    si =  XMODP_VIEW_VU;
                    break;
                case 't':
                    si =  XMODP_VIEW_PATTERN;
                    break;
                case 's':
                    si =  XMODP_VIEW_SAMPLES;
                    break;
                case 'i':
                    si =  XMODP_VIEW_INSTRUMENTS;
                    break;
                case 'm':
                    si =  XMODP_VIEW_MESSAGE;
                    break;
                case 'h':
                    si =  XMODP_VIEW_HELP;
                    break;
                default:
                    si = -1;
                    break;
            }
            if (si >= 0) {
                arguments->view = si;
            }
            break;
        case XMODP_ARG_REFRESH_RATE:
            if (arg == NULL) break;
            arguments->refresh_rate = strtol(arg, NULL, 0);
            if (arguments->refresh_rate < 1) {
                arguments->refresh_rate = 1;
            }
            break;
        case XMODP_ARG_AUDIO_ENGINE:
            if (arg == NULL) break;
            arguments->audio_engine = arg;
            break;
        case XMODP_ARG_VERSION:
            arguments->show_versions = true;
            break;
        case XMODP_ARG_SAMPLERATE:
            if (arg == NULL) break;
            arguments->samplerate = strtol(arg, NULL, 0);
            break;
        case ARGP_KEY_ARG:
#if DEBUG_ARGS
            printf("Found KEY_ARG: \"%s\"\n", arg);
#endif
            state->next--;
            arguments->files = &state->argv[state->next];
            state->next = state->argc;
            break;
        case ARGP_KEY_NO_ARGS:
#if DEBUG_ARGS
            printf("Found key no args\n");
#endif
            /* Show usage only if there's no other info argument */
            if (!arguments->show_versions) {
                argp_usage(state);
            }
        default:
#if DEBUG_ARGS
            printf("err_unknown (\"%c\"=\"%s\")\n", key, arg);
#endif
            return ARGP_ERR_UNKNOWN;
    };
    return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc };

void parse_arguments(int argc, char **argv, arguments *arguments)
{

    arguments->samplerate = XMODP_DEFAULT_SAMPLERATE;
    arguments->audio_engine = NULL;
    arguments->refresh_rate = REFRESH_MILLISECONDS;
    arguments->view = XMODP_VIEW_VU;
    arguments->show_versions = false;
    arguments->loop_mode = 0;
    arguments->use_unicode = true;
    arguments->vu_stereo = true;
    arguments->stereo_separation_percent = 50;
    arguments->volume_ramping_strength = -1;
    arguments->interpolation_filter_length = 0;
    arguments->fadeout = false;
    arguments->tempo_factor = ftoi(1, 0, XMODP_CTL_PREC);
    arguments->pitch_factor = ftoi(1, 0, XMODP_CTL_PREC);
    arguments->emulate_amiga = true;
    arguments->oplvol_factor = ftoi(1, 0, XMODP_CTL_PREC);


#if DEBUG_ARGS
    printf("Parsing:\n");
#endif
    argp_parse(&argp, argc, argv, 0, 0, arguments);
#if DEBUG_ARGS
    printf("End Parsing.\n");
#endif
#if PRINT_ARGS
    {
        int j;

        for (j = 0; arguments->files[j]; j++) {
            printf("\"%s\"\n", arguments->files[j]);
        }
    }
    exit(0);
#endif
}

