#ifndef XMODP_SPECT_H
#define XMODP_SPECT_H
#include <stdlib.h>
#include <stdbool.h>
#include <fftw3.h>
#include "xmodp_const.h"

typedef struct {
    double *in;
    size_t in_filled;
    size_t in_sz;
    bool ready;
    fftw_complex *out;
    fftw_plan p;
    double *result;
    double *hanning;
} xmodp_spectrum_analyzer_t;

void reset_spect(xmodp_spectrum_analyzer_t *a);
void init_spect(xmodp_spectrum_analyzer_t *a, size_t buf_sz, size_t stream_sz);
void cleanup_spect(xmodp_spectrum_analyzer_t *a);
void compute_spect(xmodp_spectrum_analyzer_t *a, size_t buf_sz, float *buf);

#endif

