#ifndef XMODP_OPENMPT_EFFECTS_H
#define XMODP_OPENMPT_EFFECTS_H

#define DEFAULT_EFFECT_NAME_EXT  "Ext     "
#define DEFAULT_EFFECT_NAME_NONE "        "

#define XMODP_DECLARE_EFFECT(x, y) XMODP_EFFECT_NAME_INDEX_##x,
typedef enum {
    XMODP_EFFECT_NAME_INDEX_NONE=0,
    #include "xmodp_openmpt_effect_names.h"
    XMODP_EFFECT_COUNT,
} xmodp_effect_name_indexes;
#undef XMODP_DECLARE_EFFECT

typedef struct effect_t {
    char op;
    size_t effect;
    struct effect_t *extension;
} effect_t;

extern const char *xmodp_effect_names[XMODP_EFFECT_COUNT];
extern effect_t *format_effects[];

#endif

