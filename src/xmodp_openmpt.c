#include <libopenmpt/libopenmpt.h>
#include <libopenmpt/libopenmpt_stream_callbacks_file.h>
#include "xmodp_const.h"
#include "xmodp_util.h"
#include "xmodp_openmpt_effects.h"
#include "xmodp_openmpt.h"

#define XMODP_DECLARE_CTL(x, y) y,
const char *xmodp_openmpt_ctl_str[XMODP_MPT_CTL_COUNT] = {
    NULL,
    #include "xmodp_openmpt_ctls.h"
};
#undef XMODP_DECLARE_CTL

#define XMODP_DECLARE_FORMAT(a, b, c, d) b,
const char *xmodp_openmpt_format_type[XMODP_NUM_FORMATS] = {
    NULL,
    #include "xmodp_openmpt_formats.h"
};
#undef XMODP_DECLARE_FORMAT

#define XMODP_DECLARE_FORMAT(a, b, c, d) d,
bool xmodp_openmpt_format_is_amiga[] = {
    NULL,
    #include "xmodp_openmpt_formats.h"
};
#undef XMODP_DECLARE_FORMAT

xmodp_effect_field_t *mk_effect_array(effect_t *eff)
{
    int i;
    xmodp_effect_field_t *ret;

    ret = (xmodp_effect_field_t *)malloc(
            sizeof(xmodp_effect_field_t) * 256);
    for (i = 0; i < 256; i++) {
        ret[i].effect = 0;
        ret[i].extension = NULL;
    }
    for (i = 0; eff[i].op; i++) {
            effect_t *e = &eff[i];
            if (e->extension) {
                /* This is an effect with sub-effects, recurse */
                ret[(int)(e->op)].effect = 0;
                ret[(int)(e->op)].extension = mk_effect_array(
                        e->extension);
            } else {
                ret[(int)(e->op)].effect = e->effect;
                ret[(int)(e->op)].extension = NULL;
            }
    }
    return ret;
}

void inner_destroy_effect_names(xmodp_effect_field_t *e);

void cleanup_extension(xmodp_effect_field_t **extension)
{
    inner_destroy_effect_names(*extension);
    free(*extension);
    *extension = NULL;
}

void inner_destroy_effect_names(xmodp_effect_field_t *e)
{
    int i;

    for (i = 0; i < 256; i++) {
        if (e[i].extension != NULL) {
            cleanup_extension(&(e[i].extension));
        }
    }
}

void destroy_effect_names(xmodp_openmpt_song *s)
{
    inner_destroy_effect_names(s->effect_names);
}

void init_effect_names(xmodp_openmpt_song *s)
{
    int i;

    for (i = 0; i < 256; i++) {
        s->effect_names[i].effect = 0;
        s->effect_names[i].extension = NULL;
    }

    effect_t *effect_structs = format_effects[s->format];
    if (effect_structs) {
        for (i = 0; effect_structs[i].op; i++) {
            effect_t *e = &effect_structs[i];
            if (e->extension) {
                /* This is an effect with sub-effects, recurse */
                s->effect_names[(int)(e->op)].effect = 0;
                if (s->effect_names[(int)(e->op)].extension != NULL) {
                    cleanup_extension(&(
                            s->effect_names[(int)(e->op)].extension));
                }
                s->effect_names[(int)(e->op)].extension = mk_effect_array(
                        e->extension);
            } else {
                s->effect_names[(int)(e->op)].effect = e->effect;
                s->effect_names[(int)(e->op)].extension = NULL;
            }
        }
    }
}

xmodp_format determine_format(const char *type, const char *type_long)
{
    int i;
    const char *format;

    for (i = 1; i < XMODP_NUM_FORMATS; i++) {
        format = xmodp_openmpt_format_type[i];
        if (!strcmp(type, format)) {
            return i;
        }
        if (!strncmp(type_long, format, strlen(format))) {
            return i;
        }
    }
    return XMODP_FORMAT_UNKNOWN;
}

void add_message_line(xmodp_message_t *m, char *text)
{
    xmodp_message_line_t *new_line;

    new_line = (xmodp_message_line_t *)malloc(sizeof(xmodp_message_line_t));
    new_line->next = NULL;
    new_line->text = text;

    if (m->line_list == NULL) {
        m->line_list = new_line;
    }
    else {
        xmodp_message_line_t *last;
        for (last = m->line_list; last->next != NULL; last = last->next);
        last->next = new_line;
    }
    m->num_lines++;
}

void init_song_message(xmodp_openmpt_song *s)
{
    char *m, *b;

    s->message.message = (char*)malloc(strlen(s->message_raw) + 1);
    strcpy(s->message.message, s->message_raw);
    s->message.line_list = NULL;
    s->message.num_lines = 0;

    b = s->message.message;
    for (m = b; *m != '\0'; m++) {
        if (*m == '\n') {
            *m = '\0';
            add_message_line(&s->message, b);
            b = m + 1;
        }
    }
    if (m != b) {
        add_message_line(&s->message, b);
    }
}

void init_patterns (xmodp_openmpt_song *s)
{
    int i, j, k;
    int32_t highest_pattern;
    int32_t order_pattern;
    const char *cell;
    xmodp_pattern_t *pat;
    xmodp_row_t *row;
    char cur_row[2048];

    /* determine highest pattern, to allocate a big enough array */

    highest_pattern = 0;
    for (i = 0; i < s->num_orders; i++) {
        order_pattern = openmpt_module_get_order_pattern(s->mod, i);
        if (order_pattern > highest_pattern) {
            highest_pattern = order_pattern;
        }
    }

    s->pattern_data = (xmodp_pattern_t *)malloc(
            sizeof(xmodp_pattern_t) * (highest_pattern + 1));

    /* Fill-up all the patterns */

    for (i = 0; i < highest_pattern + 1; i++) {

        pat = &s->pattern_data[i];
        pat->num_rows = openmpt_module_get_pattern_num_rows(s->mod, i);
        pat->row = (xmodp_row_t *)malloc(sizeof(xmodp_row_t) * pat->num_rows);
        for (j = 0; j < pat->num_rows; j++) {
            row = &pat->row[j];
            cur_row[0] = '\0';
            for (k = 0; k < s->num_channels; k++) {
                cell = openmpt_module_format_pattern_row_channel_command(
                    s->mod,
                    i, /* pattern index */
                    j, /* row index */
                    k, /* channel index */
                    0  /* command */
                );
                strcat(cur_row, cell);
                strcat(cur_row, " ");
                cell = openmpt_module_format_pattern_row_channel_command(
                    s->mod,
                    i, /* pattern index */
                    j, /* row index */
                    k, /* channel index */
                    1  /* command */
                );
                strcat(cur_row, cell);
                strcat(cur_row, " ");
                cell = openmpt_module_format_pattern_row_channel_command(
                    s->mod,
                    i, /* pattern index */
                    j, /* row index */
                    k, /* channel index */
                    3  /* command */
                );
                strcat(cur_row, cell);
                strcat (cur_row, " | ");
            }
            row->text = (char *)malloc(sizeof(char) * (strlen(cur_row)) + 1);
            strcpy(row->text, cur_row);
        }
    }
}

void xmodp_openmpt_log_func(const char *message, void *user) {}

int xmodp_openmpt_init(
        xmodp_openmpt_song *s, int32_t master_gain_millibel, int loop,
        int32_t stereo_separation_percent, int32_t volume_ramping_strength,
        int32_t interpolation_filter_length, bool fadeout, char *tempo_factor,
        char *pitch_factor, bool emulate_amiga, char *oplvol_factor,
        bool muted)
{
    int i;

    s->mod = openmpt_module_create2(
        openmpt_stream_get_file_callbacks(),
        s->file,
        xmodp_openmpt_log_func, /* logging function */
        NULL, /* user-defined data associated with module */
        NULL, /* error function */
        NULL, /* error func user context */
        NULL, /* error number pointer */
        NULL, /* error string pointer */
        NULL  /* map of initial ctl values */
    );
    if (!s->mod) {
        return 1;
    }
    openmpt_module_set_repeat_count(s->mod, loop);
    openmpt_module_set_render_param(
            s->mod,
            OPENMPT_MODULE_RENDER_STEREOSEPARATION_PERCENT,
            stereo_separation_percent * 2);
    openmpt_module_set_render_param(
            s->mod,
            OPENMPT_MODULE_RENDER_INTERPOLATIONFILTER_LENGTH,
            interpolation_filter_length);
    openmpt_module_set_render_param(
            s->mod,
            OPENMPT_MODULE_RENDER_VOLUMERAMPING_STRENGTH,
            volume_ramping_strength);
    openmpt_module_set_render_param(
            s->mod,
            OPENMPT_MODULE_RENDER_MASTERGAIN_MILLIBEL,
            muted ? INT32_MIN : master_gain_millibel);


#define CTL_STR(x) xmodp_openmpt_ctl_str[XMODP_MPT_CTL_##x]

    openmpt_module_ctl_set(s->mod, CTL_STR(PLAY_AT_END),
            fadeout ? "fadeout" : "stop");
    openmpt_module_ctl_set(s->mod, CTL_STR(TEMPO_FACTOR), tempo_factor);
    openmpt_module_ctl_set(s->mod, CTL_STR(PITCH_FACTOR), pitch_factor);
    openmpt_module_ctl_set(s->mod, CTL_STR(EMULATE_AMIGA),
            emulate_amiga ? "1" : "0");
    openmpt_module_ctl_set(s->mod, CTL_STR(OPL_VOLUME_FACTOR), oplvol_factor);

#undef CTL_STR

    s->num_orders = openmpt_module_get_num_orders(s->mod);
    s->num_samples = openmpt_module_get_num_samples(s->mod);
    for (i = 0; i < s->num_samples; i++) {
        s->sample[i].name = openmpt_module_get_sample_name(s->mod, i);
    }
    s->num_instruments = openmpt_module_get_num_instruments(s->mod);
    for (i = 0; i < s->num_instruments; i++) {
        s->instrument[i].name = openmpt_module_get_instrument_name(s->mod, i);
    }
    s->num_channels = openmpt_module_get_num_channels(s->mod);
    s->duration_seconds = openmpt_module_get_duration_seconds(s->mod);
    s->title = openmpt_module_get_metadata(s->mod, "title");
    s->type = openmpt_module_get_metadata(s->mod, "type");
    s->type_long = openmpt_module_get_metadata(s->mod, "type_long");
    s->originaltype = openmpt_module_get_metadata(s->mod, "originaltype");
    s->originaltype_long = openmpt_module_get_metadata(
            s->mod, "originaltype_long");
    s->container = openmpt_module_get_metadata(s->mod, "container");
    s->container_long = openmpt_module_get_metadata(s->mod, "container_long");
    s->format = determine_format(
            strlen(s->originaltype) ? s->originaltype : s->type,
            strlen(s->originaltype_long) ? s->originaltype_long : s->type_long);
    s->is_amiga = xmodp_openmpt_format_is_amiga[s->format];
    s->tracker = openmpt_module_get_metadata(s->mod, "tracker");
    s->artist = openmpt_module_get_metadata(s->mod, "artist");
    s->message_raw = openmpt_module_get_metadata(s->mod, "message_raw");
    init_song_message(s);
    init_patterns(s);
    for (i = 0; i < s->num_channels; i++) {
        s->channel[i].note = NULL;
        s->channel[i].effect = NULL;
        s->channel[i].effect_value = NULL;
        s->channel[i].instrument_idx = 0;
        s->channel[i].vu_mono = 0;
        s->channel[i].vu_left = 0;
        s->channel[i].vu_right = 0;
    }

    /* openmpt_module_get_num_patterns(s->mod); */

    s->current_subsong = openmpt_module_get_selected_subsong(s->mod);
    s->num_subsongs = openmpt_module_get_num_subsongs(s->mod);
    init_effect_names(s);

    return 0;
}

size_t xmodp_openmpt_read(xmodp_openmpt_song *s, xmodp_buffer *buf, int rate)
{
    size_t count;
    count = openmpt_module_read_float_stereo(
            s->mod,
            rate,
            XMODP_BUFFERSIZE,
            buf->left,
            buf->right);
    render_buf(buf->stereo, buf->mono, buf->left, buf->right, count);
    buf->filled_amount = count;
    return count;
}

void xmodp_openmpt_update(xmodp_openmpt_song *s)
{
    uint16_t i;
    const char *new_note;
    int new_instrument_idx;

    s->order = openmpt_module_get_current_order(s->mod);
    s->current_pos_seconds = openmpt_module_get_position_seconds(s->mod);
    s->speed = openmpt_module_get_current_speed(s->mod);
    s->tempo = openmpt_module_get_current_tempo(s->mod);
    s->pattern = openmpt_module_get_current_pattern(s->mod);
    s->num_rows = openmpt_module_get_pattern_num_rows(s->mod, s->pattern);
    s->row = openmpt_module_get_current_row(s->mod);
    for (i = 0; i < s->num_channels; i++) {
        /* libopenmpt mixes up left and right? FIXED RECENTLY */
        s->channel[i].vu_right = openmpt_module_get_current_channel_vu_left(
                s->mod, i);
        s->channel[i].vu_left = openmpt_module_get_current_channel_vu_right(
                s->mod, i);
        s->channel[i].vu_mono = openmpt_module_get_current_channel_vu_mono(
                s->mod, i);
        s->channel[i].effect =
                openmpt_module_format_pattern_row_channel_command(
                        s->mod,
                        s->pattern,
                        s->row,
                        i,
                        OPENMPT_MODULE_COMMAND_EFFECT);
        s->channel[i].effect_value =
                openmpt_module_format_pattern_row_channel_command(
                        s->mod,
                        s->pattern,
                        s->row,
                        i,
                        OPENMPT_MODULE_COMMAND_PARAMETER);
        new_note =
                openmpt_module_format_pattern_row_channel_command(
                        s->mod,
                        s->pattern,
                        s->row,
                        i,
                        OPENMPT_MODULE_COMMAND_NOTE);
        if (new_note[0] != '.' || s->channel[i].note == NULL) {
            s->channel[i].note = new_note;
        }
        new_instrument_idx =
                openmpt_module_get_pattern_row_channel_command(
                        s->mod,
                        s->pattern,
                        s->row,
                        i,
                        OPENMPT_MODULE_COMMAND_INSTRUMENT);
        if (new_instrument_idx > 0) {
            s->channel[i].instrument_idx = new_instrument_idx;
        }
    }
    s->current_play_channels = openmpt_module_get_current_playing_channels(
            s->mod);
}

const char * extension_effect_name(xmodp_effect_field_t *e, const char *value)
{
    xmodp_effect_field_t *f;

    f = &(e[(int)(value[0])]);
    if (f->extension != NULL) {
        return extension_effect_name(e->extension, &(value[1]));
    }
    if (f->effect) {
        return xmodp_effect_names[f->effect];
    }
    return DEFAULT_EFFECT_NAME_EXT;
}

const char * human_effect(
        xmodp_openmpt_song *s, const char *effect, const char *value)
{
    xmodp_effect_field_t *f;

    f = &(s->effect_names[(int)(effect[0])]);
    if (f->extension != NULL) {
        return extension_effect_name(f->extension, value);
    }
    if (f->effect) {
        return xmodp_effect_names[f->effect];
    }
    if (effect[0] != '.' && effect[0] != '?') {
        return effect;
    }
    return "";
}

const char *xmodp_libopenmpt_get_string(const char *s)
{
    return openmpt_get_string(s);
}

void xmodp_openmpt_destroy(xmodp_openmpt_song *s)
{
    openmpt_module_destroy(s->mod);
    fclose(s->file);
    destroy_effect_names(s);
}

