#define _XOPEN_SOURCE_EXTENDED 1

#include <stdlib.h>
#include <stdbool.h>
#include <ncurses.h>
#include <string.h>
#include "xmodp_screen.h"
#include "xmodp_tk.h"

void print_check(bool unicode, bool val)
{
    val ? (unicode ? addwstr(L"\u2713") : addch('X')) : addch(' ');
}

void x_printw(char *s, int max) {
    size_t len;
    char *tmp;

    len = strlen(s);
    if (len < max) {
        printw(s);
        return;
    }
    tmp = (char*)malloc(max + 1);
    strncpy(tmp, s, max);
    tmp[max] = '\0';
    printw(tmp);
    free(tmp);
}

void paint_vu_stereo_horiz(
        int y, int x, int width, float vl, float vr, bool unicode,
        xmodp_colors color_bg, xmodp_colors color_low,
        xmodp_colors color_med, xmodp_colors color_high)
{
    int i;
    int py;
    int px;
    bool share_middle;
    bool left_energy = false;

    int vu_char = '|';
    wchar_t *vu_charw = L"\u25ae";

    int v_l;
    int lcutoff_low;
    int lcutoff_med;
    int lwidth;

    int v_r;
    int rcutoff_low;
    int rcutoff_med;
    int rwidth;

    lwidth = width / 2;
    rwidth = width - lwidth;
    if (width % 2) {
        lwidth += 1;
        share_middle = true;
    } else {
        share_middle = false;
    }
    lcutoff_low = lwidth / 2;
    lcutoff_med = lcutoff_low + (lcutoff_low / 2);
    rcutoff_low = rwidth / 2;
    rcutoff_med = rcutoff_low + (rcutoff_low / 2);

    v_l = (int)(vl * lwidth + 0.5);
    v_r = (int)(vr * rwidth + 0.5);

    /* left side */

    move(y, x);
    for (i = 0; i < lwidth; i++) {
        if (i < (lwidth - v_l)) {
            left_energy = false;
            xmodp_set_color(color_bg);
            unicode ? addwstr(vu_charw) : addch(vu_char);
            continue;
        }
        left_energy = true;
        if (i > (lwidth - lcutoff_low)) {
            xmodp_set_color(color_low);
        } else if (i > (lwidth - lcutoff_med)) {
            xmodp_set_color(color_med);
        } else {
            xmodp_set_color(color_high);
        }
        unicode ? addwstr(vu_charw) : addch(vu_char);
    }

    /* right side */

    if (share_middle) {
        // move back by one
        getyx(stdscr, py, px);
        move(py, px - 1);
    }
    for (i = 0; i < rwidth; i++) {
        if (i >= v_r) {
            if (i == 0 && share_middle == true && left_energy == true) {
                move(py, px);
            } else {
                xmodp_set_color(color_bg);
                unicode ? addwstr(vu_charw) : addch(vu_char);
            }
            continue;
        }
        if (i < rcutoff_low) {
            xmodp_set_color(color_low);
        } else if (i < rcutoff_med) {
            xmodp_set_color(color_med);
        } else {
            xmodp_set_color(color_high);
        }
        unicode ? addwstr(vu_charw) : addch(vu_char);
    }
}

void paint_vu_horiz(
        int y, int x, int width, float vm, bool unicode,
        xmodp_colors color_bg, xmodp_colors color_low,
        xmodp_colors color_med, xmodp_colors color_high)
{
    int v_m;
    int i;
    int cutoff_low;
    int cutoff_med;
    int vu_char = '|';
    wchar_t *vu_charw = L"\u25ae";


    cutoff_low = width / 2;
    cutoff_med = cutoff_low + (cutoff_low / 2);

    v_m = (int)(vm * width + 0.5);

    move(y, x);
    for (i = 0; i < width; i++) {
        if (i >= v_m) {
            xmodp_set_color(color_bg);
            unicode ? addwstr(vu_charw) : addch(vu_char);
            continue;
        }
        if (i < cutoff_low) {
            xmodp_set_color(color_low);
        } else if (i < cutoff_med) {
            xmodp_set_color(color_med);
        } else {
            xmodp_set_color(color_high);
        }
        unicode ? addwstr(vu_charw) : addch(vu_char);
    }
}

void paint_blank(int y1, int x1, int y2, int x2, char *message)
{
    int y;
    size_t len;

    len = x2 - x1;
#if 0
    char *line;
    line = (char*)malloc(len + 1);
    memset(line, ' ', len);
    line[len] = '\0';
    for (y = y1; y < y2; y++) {
        mvprintw(y, x1, line);
    }
    free(line);

#else

    /* naive approach, is it faster ? */
    {
        int x;

        for (y = y1; y < y2; y++) {
            move(y, x1);
            for (x = x1; x < x2; x++) {
                addch(' ');
            }
        }
    }

#endif

    if (message != NULL) {
        int max_str;
        int message_len;
        int x_pos;

        max_str = len - 2;
        if (max_str > 0) {
            message_len = strlen(message);
            if (message_len > max_str) {
                message_len = max_str;
            }
            x_pos = (x2 - ((len + 0.5) / 2) - (message_len + 0.5) / 2) + 1;
            move(y2 - ((y2 - y1 + 0.5) / 2), x_pos);
            x_printw(message, max_str);
        }
    }
}

void paint_box(int y1, int x1, int y2, int x2, int tl_col, int br_col)
{
    int i;


    move(y1, x1);
    xmodp_set_color(tl_col);
    addch(ACS_ULCORNER);
    for(i = 0; i < x2 - x1 - 1; i++) {
        addch(ACS_HLINE);
    }
    xmodp_set_color(br_col);
    addch(ACS_URCORNER);
    for(i = 0; i < y2 - y1 - 1; i++) {
        move(y1 + 1 + i, x1);
        xmodp_set_color(tl_col);
        addch(ACS_VLINE);
        move(y1 + 1 + i, x2);
        xmodp_set_color(br_col);
        addch(ACS_VLINE);
    }
    move(y2, x1);
    xmodp_set_color(tl_col);
    addch(ACS_LLCORNER);
    xmodp_set_color(br_col);
    for(i = 0; i < x2 - x1 - 1; i++) {
        addch(ACS_HLINE);
    }
    addch(ACS_LRCORNER);
}

