#include <locale.h>
#include <signal.h>
#include <time.h>
#include "config.h"

#include "xmodp_const.h"
#include "xmodp_globals.h"
#include "xmodp_sound.h"
#include "xmodp_paint.h"
#include "xmodp_input.h"
#include "xmodp_spect.h"
#include "xmodp_arg.h"
#include "xmodp_util.h"

void print_version_info(xmodp_state *v)
{
    printf("%s - v%s\n", TITLE_HEADER, XMODP_VERSION);
    printf("libOpenMPT - v%s\n", v->lib_meta.libopenmpt_version);
}

void clean_exit(xmodp_state *v, int code, char *message)
{
    cleanup_sound(v->audio_engine_used, &v->audio_vars);
    cleanup_video(&v->scr);
#if USE_SPECTRUM_ANALYZER
    cleanup_spect(&v->a);
#endif
    if (message != NULL) {
        printf(message);
    }
    exit(code);
}

void throttle_pause(xmodp_state *v)
{
    struct timespec pause_sleeper;

    pause_sleeper.tv_sec = 1;
    pause_sleeper.tv_nsec = 0;
    nanosleep(&pause_sleeper, NULL);
}

extern xmodp_state v;

int main(int argc, char **argv)
{
    arguments arguments;
    int song_num;
    bool quit_on_song_load_error = false;

    parse_arguments(argc, argv, &arguments);

    /* Set initial state */

    set_library_versions(&v);
    if (arguments.show_versions) {
        print_version_info(&v);
        exit(0);
    }
    v.quit = false;
    v.updating = false;
    v.scr.initialized = false;
    v.muted = false;
    v.paused = false;
    v.audio_engine_used = set_audio_engine(arguments.audio_engine);
    v.samplerate = arguments.samplerate;
    v.unicode = arguments.use_unicode;
    v.repeat = arguments.refresh_rate;
    v.main_view = arguments.view;
    set_view_title(&v);
    v.vu.stereo = arguments.vu_stereo;
    v.master_gain_millibel = 0;
    v.loop = arguments.loop_mode;
    v.stereo_separation_percent = arguments.stereo_separation_percent;
    v.volume_ramping_strength = arguments.volume_ramping_strength;
    v.interpolation_filter_length = arguments.interpolation_filter_length;
    v.fadeout = arguments.fadeout;
    v.tempo_factor = arguments.tempo_factor;
    v.pitch_factor = arguments.pitch_factor;
    v.emulate_amiga = arguments.emulate_amiga;
    v.oplvol_factor = arguments.oplvol_factor;

    /* Initialize sound engine */

    init_sound(v.audio_engine_used, v.samplerate, &v.audio_vars);

    /* Initialize ncurses */

    setlocale(LC_ALL, "");
    init_video(&v.scr);

    /* Load song */

    for (song_num = 0; arguments.files[song_num]; song_num++) {
        char tempo_factor_s[64];
        char pitch_factor_s[64];
        char oplvol_factor_s[64];
        v.s.filename = arguments.files[song_num];
        v.s.file = fopen(v.s.filename, "rb");
        if (xmodp_openmpt_init(
                &v.s,
                v.master_gain_millibel,
                v.loop,
                v.stereo_separation_percent,
                v.volume_ramping_strength,
                v.interpolation_filter_length,
                v.fadeout,
                itofs(tempo_factor_s, 64, v.tempo_factor, XMODP_CTL_PREC),
                itofs(pitch_factor_s, 64, v.pitch_factor, XMODP_CTL_PREC),
                v.emulate_amiga,
                itofs(oplvol_factor_s, 64, v.oplvol_factor, XMODP_CTL_PREC),
                v.muted)) {
            if (quit_on_song_load_error) {
                char msg[1024];
                snprintf(msg, 1023,
                        "Error loading song \"%s\".\n", v.s.filename);
                clean_exit(&v, 1, msg);
            } else {
                continue;
            }
        }
        /* Reset any global variables that need it on song change */
        v.sample.idx = 0;
        v.instrument.idx = 0;
        v.message.idx = 0;

        v.song_ended = false;
        v.scr.main_need_refresh = true;

#if USE_SPECTRUM_ANALYZER
        init_spect(&v.a, SA_SAMPLE_SZ, XMODP_BUFFERSIZE);
#endif

        /* Set timer to call update() at regular intervals */

        signal(SIGALRM, update);
        if (set_timer(ITIMER_REAL, v.repeat) == -1) {
            char msg[1024];

            perror("set_timer");
            snprintf(msg, 1023, "ERROR: set_timer");
            clean_exit(&v, 1, msg);
        }

        while (!v.quit && !v.song_ended)
        {
            if (!v.paused) {
                handle_sound(&v);
                /* update song metadata */

                xmodp_openmpt_update(&v.s);
#if USE_SPECTRUM_ANALYZER
                handle_spectrum_analyzer(&v);
#endif
            }
            else {
                throttle_pause(&v);
            }
            handle_input(&v);
        }
        v.song_ended = true;
        xmodp_openmpt_destroy(&v.s);
        if(v.quit) {
            break;
        }
    }
    clean_exit(&v, 0, NULL);
}

