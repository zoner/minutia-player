#ifndef XMODP_INPUT_H
#define XMODP_INPUT_H

bool handle_input_vu(xmodp_state *v, int c);
bool handle_input_help(xmodp_state *v, int c);
bool handle_input_samples(xmodp_state *v, int c);
bool handle_input_instruments(xmodp_state *v, int c);
bool handle_input_message(xmodp_state *v, int c);
bool handle_input_pattern(xmodp_state *v, int c);
bool handle_input_analyzer(xmodp_state *v, int c);
bool handle_input(xmodp_state *v);

#endif
