#ifndef XMODP_TK_H
#define XMODP_TK_H

#include <stdbool.h>

void print_check(bool unicode, bool val);
void x_printw(char *s, int max);
void paint_vu_horiz(
        int y, int x, int width, float vm, bool unicode,
        xmodp_colors color_bg, xmodp_colors color_low,
        xmodp_colors color_med, xmodp_colors color_high);
void paint_vu_stereo_horiz(
        int y, int x, int width, float vl, float vr, bool unicode,
        xmodp_colors color_bg, xmodp_colors color_low,
        xmodp_colors color_med, xmodp_colors color_high);
void paint_blank(int y1, int x1, int y2, int x2, char *message);
void paint_box(int y1, int x1, int y2, int x2, int tl_col, int br_col);

#endif
