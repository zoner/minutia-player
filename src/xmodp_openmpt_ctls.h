
XMODP_DECLARE_CTL(  SUBSONG,            "subsong")
XMODP_DECLARE_CTL(  PLAY_AT_END,        "play.at_end")
XMODP_DECLARE_CTL(  TEMPO_FACTOR,       "play.tempo_factor")
XMODP_DECLARE_CTL(  PITCH_FACTOR,       "play.pitch_factor")
XMODP_DECLARE_CTL(  EMULATE_AMIGA,      "render.resampler.emulate_amiga")
XMODP_DECLARE_CTL(  OPL_VOLUME_FACTOR,  "render.opl.volume_factor")

