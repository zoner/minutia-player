#include <ncurses.h>
#include "xmodp_screen.h"

xmodp_color_t xmodp_color[XMODP_COL_COUNT];

#define COL_BG (COLOR_WHITE)
#define COL_BG_T (COLOR_BLACK)
#define COL_BG_T_CUR (COLOR_BLUE)

#define XMODP_DECLARE_COLOR(a, b, c, d) {b, c, d},
xmodp_color_t default_colors[XMODP_COL_COUNT] = {
    #include "xmodp_colors.h"
};
#undef XMODP_DECLARE_COLOR

void xmodp_init_color(xmodp_colors color_idx, char f, char b, bool bold)
{
    init_pair(color_idx + 1, f, b);
    xmodp_color[color_idx].fg = f;
    xmodp_color[color_idx].bg = b;
    xmodp_color[color_idx].bold = bold;
}

void xmodp_set_color(xmodp_colors color_idx)
{
    attron(COLOR_PAIR(color_idx + 1));
    xmodp_color[color_idx].bold ? attron(A_BOLD) : attroff(A_BOLD);
}

void init_video(xmodp_screen *s)
{
    initscr();
    curs_set(0); /* set cursor to invisible */
    raw();
    noecho();
    cbreak();
    nodelay(stdscr, true);
    keypad(stdscr, true);
    if (has_colors() == false) {
        s->bw = true;
    }
    if (!s->bw) {
        xmodp_colors i;

        start_color();
        init_color(COLOR_WHITE, 600,600,600);

        for (i = 0; i < XMODP_COL_COUNT; i++) {
            xmodp_init_color(
                    i,
                    default_colors[i].fg,
                    default_colors[i].bg,
                    default_colors[i].bold);
        }
    }
    bkgd(COLOR_PAIR(XMODP_COL_MAIN));
    refresh();
    s->initialized = true;
}

void cleanup_video(xmodp_screen *s)
{
    if (s->initialized) {
        s->initialized = false;
        endwin();
    }
}

void screen_size_change(xmodp_screen *scr, int y, int x)
{
    scr->sy = y;
    scr->sx = x;
}

bool check_screen_size(xmodp_screen *scr, bool update)
{
    int cur_sy;
    int cur_sx;

    getmaxyx(stdscr, cur_sy, cur_sx);
    if ((cur_sy != scr->sy) || (cur_sx != scr->sx)) {
        if (update) {
            screen_size_change(scr, cur_sy, cur_sx);
        }
        return true;
    }
    return false;
}

