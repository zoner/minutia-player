#ifndef XMODP_PULSEAUDIO_H
#define XMODP_PULSEAUDIO_H

#include <pulse/pulseaudio.h>
#include <pulse/simple.h>

typedef struct {
    pa_simple *stream;
    int samplerate;
} pulse_vars;

int init_pulseaudio(pulse_vars *p);
int cleanup_pulseaudio(pulse_vars *p);

#endif

