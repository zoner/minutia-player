#include <pulse/pulseaudio.h>
#include <pulse/simple.h>
#include "xmodp_pulseaudio.h"

int init_pulseaudio(pulse_vars *p)
{
    pa_sample_spec ss;
    ss.format = PA_SAMPLE_FLOAT32NE;
    ss.rate = p->samplerate;
    ss.channels = 2;

    p->stream = pa_simple_new(
        NULL, /* Use the default server */
        "xmodp", /* application's name */
        PA_STREAM_PLAYBACK, /* ? */
        NULL, /* Use default device */
        "xmodp", /* stream's description */
        &ss, /* sample format */
        NULL, /* use default channel map */
        NULL, /* buffering attributes */
        NULL /* error codes */
    );
    if (!p->stream) {
        return 1;
    }
    return 0;
}

int cleanup_pulseaudio(pulse_vars *p)
{
    if (p->stream) {
        if (pa_simple_drain(p->stream, NULL) < 0) {
            return 1;
        }
        pa_simple_free(p->stream);
        p->stream = NULL;
    }
    return 0;
}

