#ifndef XMODP_CONST_H
#define XMODP_CONST_H

#define USE_SPECTRUM_ANALYZER (0)
#define REFRESH_MILLISECONDS 10
#define XMODP_RESIZE_REDRAW_DELAY (0)

#define XMODP_NAME "minutia"
#define XMODP_VERSION "0.1"
#define TITLE_HEADER "Minutia Player"
#define TITLE_VU "Channels"
#define TITLE_HELP "Help!"
#define TITLE_TRACKS "Tracks"
#define TITLE_ANALYZER "Spectrum Analyzer"
#define TITLE_OPTIONS "Options"
#define TITLE_SAMPLES "Samples"
#define TITLE_INSTRUMENTS "Instruments"
#define TITLE_MESSAGE "Message"

#define STR_IMPL_(x) #x
#define STR(x) STR_IMPL_(x)

#define XMODP_DEFAULT_AUDIO_ENGINE "pulse"
#define XMODP_DEFAULT_AUDIO_ENGINE_NUM AUDIO_ENGINE_PULSEAUDIO

#define XMODP_DEFAULT_SAMPLERATE 48000
#define XMODP_BUFFERSIZE (480)
#define SA_SAMPLE_SZ (512)

#define XMODP_VOLUME_INCREMENT (100)
#define XMODP_CTL_PREC (1)

#define META_HEIGHT (12)
#define MAX_META_LINE_LEN (1022)

#define XMODP_KEY_HELP          ('h')
#define XMODP_KEY_HELP2         ('?')
#define XMODP_KEY_QUIT          ('q')
#define XMODP_KEY_SAMPLES       ('s')
#define XMODP_KEY_INSTRUMENTS   ('i')
#define XMODP_KEY_MESSAGE       ('g')
#define XMODP_KEY_TRACKS        ('t')
#define XMODP_KEY_OPTIONS       ('o')
#define XMODP_KEY_VU            ('v')
#define XMODP_KEY_ANALYZER      ('z')
#define XMODP_KEY_MUTE          ('m')
#define XMODP_KEY_PAUSE         ('p')
#define XMODP_KEY_PAUSE2        (' ')
#define XMODP_KEY_SUBSONG_PREV  ('<')
#define XMODP_KEY_SUBSONG_NEXT  ('>')
#define XMODP_KEY_ORDER_PREV    (',')
#define XMODP_KEY_ORDER_NEXT    ('.')
#define XMODP_KEY_VOL_UP        ('+')
#define XMODP_KEY_VOL_DOWN      ('-')
#define XMODP_KEY_LOOP          ('l')
#define XMODP_KEY_VU_STEREO     ('w')
#define XMODP_KEY_RAMP          ('r')
#define XMODP_KEY_INTERP        ('y')
#define XMODP_KEY_STEREO_SEP    ('e')
#define XMODP_KEY_SONG_NEXT     ('n')

#define XMODP_KEY_FADEOUT       ('f')
#define XMODP_KEY_TEMPO_UP      (']')
#define XMODP_KEY_TEMPO_DOWN    ('[')
#define XMODP_KEY_PITCH_UP      ('}')
#define XMODP_KEY_PITCH_DOWN    ('{')
#define XMODP_KEY_AMIGA_EMU     ('a')
#define XMODP_KEY_OPLVOL_UP     (')')
#define XMODP_KEY_OPLVOL_DOWN   ('(')


#define XMODP_ARG_VIEW          ('d')
#define XMODP_ARG_REFRESH_RATE  ('r')
#define XMODP_ARG_AUDIO_ENGINE  ('e')
#define XMODP_ARG_SAMPLERATE    ('s')
#define XMODP_ARG_VERSION       ('v')
#define XMODP_ARG_LOOP_MODE     ('l')
#define XMODP_ARG_UNICODE       ('u')
#define XMODP_ARG_STEREO_VU     ('w')
#define XMODP_ARG_STEREO_SEP    ('y')
#define XMODP_ARG_VOL_RAMPING   ('g')
#define XMODP_ARG_FILTER        ('i')
#define XMODP_ARG_FADEOUT       ('f')
#define XMODP_ARG_TEMPO_FACTOR  ('t')
#define XMODP_ARG_PITCH_FACTOR  ('p')
#define XMODP_ARG_AMIGA_EMU     ('a')
#define XMODP_ARG_OPLVOL_FACTOR ('o')

#endif

