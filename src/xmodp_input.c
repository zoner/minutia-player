#include <stdbool.h>
#include <ncurses.h>
#include "xmodp_globals.h"
#include "xmodp_const.h"
#include "xmodp_input.h"

typedef bool (*handle_input_ptr_t)(xmodp_state*, int);

handle_input_ptr_t handle_input_func[XMODP_NUM_VIEWS] = {
    handle_input_vu,
    handle_input_samples,
    handle_input_instruments,
    handle_input_message,
    handle_input_help,
    handle_input_pattern,
    handle_input_analyzer,
};

bool handle_input_vu(xmodp_state *v, int c)
{
    switch (c) {
        case XMODP_KEY_VU_STEREO:
            v->vu.stereo = !(v->vu.stereo);
            return true;
        default:
            break;
    }
    return false;
}

bool handle_input_help(xmodp_state *v, int c)
{
    int height;
    static uint16_t help_sz = 0;

    if (!help_sz) {
        for(;help_text[help_sz] != NULL; help_sz++);
    }

    switch (c) {
        case KEY_DOWN:
            height = v->meta.pos_y - v->scr.main_pos_y - 2;
            if (v->help.idx + height < help_sz) {
                v->help.idx++;
                v->scr.main_need_refresh = true;
            }
            return true;
        case KEY_UP:
            if (v->help.idx > 0) {
                v->help.idx--;
                v->scr.main_need_refresh = true;
            }
            return true;
        default:
            break;
    }
    return false;
}

bool handle_input_samples(xmodp_state *v, int c)
{
    int height;

    switch (c) {
        case KEY_DOWN:
            height = v->meta.pos_y - v->scr.main_pos_y - 2;
            if (v->sample.idx + height < v->s.num_samples) {
                v->sample.idx++;
                v->scr.main_need_refresh = true;
            }
            return true;
        case KEY_UP:
            if (v->sample.idx > 0) {
                v->sample.idx--;
                v->scr.main_need_refresh = true;
            }
            return true;
        default:
            break;
    }
    return false;
}

bool handle_input_instruments(xmodp_state *v, int c)
{
    int height;

    switch (c) {
        case KEY_DOWN:
            height = v->meta.pos_y - v->scr.main_pos_y - 2;
            if (v->instrument.idx + height < v->s.num_instruments) {
                v->instrument.idx++;
                v->scr.main_need_refresh = true;
            }
            return true;
        case KEY_UP:
            if (v->instrument.idx > 0) {
                v->instrument.idx--;
                v->scr.main_need_refresh = true;
            }
            return true;
        default:
            break;
    }
    return false;
}

bool handle_input_message(xmodp_state *v, int c)
{
    int height;

    switch (c) {
        case KEY_DOWN:
            height = v->meta.pos_y - v->scr.main_pos_y - 2;
            if (v->message.idx + height < v->s.message.num_lines) {
                v->message.idx++;
                v->scr.main_need_refresh = true;
            }
            return true;
        case KEY_UP:
            if (v->message.idx > 0) {
                v->message.idx--;
                v->scr.main_need_refresh = true;
            }
            return true;
        default:
            break;
    }
    return false;
}

bool handle_input_pattern(xmodp_state *v, int c)
{
    return false;
}

bool handle_input_analyzer(xmodp_state *v, int c)
{
    return false;
}

bool handle_input(xmodp_state *v)
{
    int c;
    int32_t gain;
    int32_t ramp;
    int32_t sep;
    int32_t interp;
    char ctl_s[64];

    c = getch();
    if (c == ERR) {
        return false;
    }
    if (handle_input_func[v->main_view](v, c)) {
        return true;
    }
    switch (c) {
        case XMODP_KEY_QUIT:
            v->quit = true;
            return true;
        case XMODP_KEY_HELP:
        case XMODP_KEY_HELP2:
            v->main_view = XMODP_VIEW_HELP;
            set_view_title(v);
            v->scr.main_need_refresh = true;
            return true;
        case XMODP_KEY_VU:
            v->main_view = XMODP_VIEW_VU;
            set_view_title(v);
            v->scr.main_need_refresh = true;
            return true;
        case XMODP_KEY_SAMPLES:
            v->main_view = XMODP_VIEW_SAMPLES;
            set_view_title(v);
            v->scr.main_need_refresh = true;
            return true;
        case XMODP_KEY_INSTRUMENTS:
            v->main_view = XMODP_VIEW_INSTRUMENTS;
            set_view_title(v);
            v->scr.main_need_refresh = true;
            return true;
        case XMODP_KEY_MESSAGE:
            v->main_view = XMODP_VIEW_MESSAGE;
            set_view_title(v);
            v->scr.main_need_refresh = true;
            return true;
        case XMODP_KEY_TRACKS:
            v->main_view = XMODP_VIEW_PATTERN;
            set_view_title(v);
            v->scr.main_need_refresh = true;
            return true;
#if USE_SPECTRUM_ANALYZER
        case XMODP_KEY_ANALYZER:
            v->main_view = XMODP_VIEW_ANALYZER;
            set_view_title(v);
            v->scr.main_need_refresh = true;
            return true;
#endif
        case XMODP_KEY_OPTIONS:
            /* TODO: Options view */
            break;
        case XMODP_KEY_PAUSE:
        case XMODP_KEY_PAUSE2:
            v->paused = !v->paused;
            return true;
        case XMODP_KEY_RAMP:
            openmpt_module_get_render_param(
                    v->s.mod,
                    OPENMPT_MODULE_RENDER_VOLUMERAMPING_STRENGTH,
                    &ramp);
            switch(ramp) {
                case 10:
                    ramp = -1;
                    break;
                default:
                    ramp++;
                    break;
            }
            v->volume_ramping_strength = ramp;
            openmpt_module_set_render_param(
                    v->s.mod,
                    OPENMPT_MODULE_RENDER_VOLUMERAMPING_STRENGTH,
                    v->volume_ramping_strength);
            break;
        case XMODP_KEY_INTERP:
            openmpt_module_get_render_param(
                    v->s.mod,
                    OPENMPT_MODULE_RENDER_INTERPOLATIONFILTER_LENGTH,
                    &interp);
            switch(interp) {
                case 0:
                    interp = 1;
                    break;
                case 1:
                    interp = 2;
                    break;
                case 2:
                    interp = 4;
                    break;
                case 4:
                    interp = 8;
                    break;
                default:
                    interp = 1;
                    break;
            }
            v->interpolation_filter_length = interp;
            openmpt_module_set_render_param(
                    v->s.mod,
                    OPENMPT_MODULE_RENDER_INTERPOLATIONFILTER_LENGTH,
                    v->interpolation_filter_length);
            break;
        case XMODP_KEY_STEREO_SEP:
            openmpt_module_get_render_param(
                    v->s.mod,
                    OPENMPT_MODULE_RENDER_STEREOSEPARATION_PERCENT,
                    &sep);
            sep /= 2;
            switch(sep) {
                case 0:
                    sep = 50;
                    break;
                case 50:
                    sep = 100;
                    break;
                default:
                    sep = 0;
                    break;
            }
            v->stereo_separation_percent = sep;
            openmpt_module_set_render_param(
                    v->s.mod,
                    OPENMPT_MODULE_RENDER_STEREOSEPARATION_PERCENT,
                    v->stereo_separation_percent * 2);
            break;
        case XMODP_KEY_LOOP:
            switch (openmpt_module_get_repeat_count(v->s.mod)) {
                case 0:
                    v->loop = -1;
                    break;
                case -1:
                    v->loop = 1;
                    break;
                case 1:
                    v->loop = 2;
                    break;
                case 2:
                    v->loop = 0;
                    break;
            }
            openmpt_module_set_repeat_count(v->s.mod, v->loop);
            return true;
        case XMODP_KEY_MUTE:
            v->muted = !v->muted;
            if (v->muted) {
                gain = INT32_MIN;
            }
            else {
                gain = v->master_gain_millibel;
            }
            openmpt_module_set_render_param(
                    v->s.mod,
                    OPENMPT_MODULE_RENDER_MASTERGAIN_MILLIBEL,
                    gain);
            break;
        case XMODP_KEY_VOL_UP:
            if (v->master_gain_millibel < INT32_MAX - XMODP_VOLUME_INCREMENT) {
                v->master_gain_millibel += XMODP_VOLUME_INCREMENT;
                if (!v->muted) {
                    openmpt_module_set_render_param(
                            v->s.mod,
                            OPENMPT_MODULE_RENDER_MASTERGAIN_MILLIBEL,
                            v->master_gain_millibel);
                }
            }
            return true;
        case XMODP_KEY_VOL_DOWN:
            if (v->master_gain_millibel > INT32_MIN + XMODP_VOLUME_INCREMENT) {
                v->master_gain_millibel -= XMODP_VOLUME_INCREMENT;
                if (!v->muted) {
                    openmpt_module_set_render_param(
                            v->s.mod,
                            OPENMPT_MODULE_RENDER_MASTERGAIN_MILLIBEL,
                            v->master_gain_millibel);
                }
            }
            return true;
        case XMODP_KEY_SUBSONG_NEXT:
            if (v->s.current_subsong < v->s.num_subsongs - 1) {
                v->s.current_subsong++;
                openmpt_module_select_subsong(v->s.mod, v->s.current_subsong);
            }
            return true;
        case XMODP_KEY_SUBSONG_PREV:
            if (v->s.current_subsong > 0) {
                v->s.current_subsong--;
                openmpt_module_select_subsong(v->s.mod, v->s.current_subsong);
            }
            return true;
        case XMODP_KEY_ORDER_PREV:
            openmpt_module_set_position_order_row(
                    v->s.mod,
                    v->s.order - 1,
                    0);
            return true;
        case XMODP_KEY_ORDER_NEXT:
            openmpt_module_set_position_order_row(
                    v->s.mod,
                    v->s.order + 1,
                    0);
            return true;
        case XMODP_KEY_SONG_NEXT:
            v->song_ended = true;
            break;
        case XMODP_KEY_FADEOUT:
            v->fadeout = !v->fadeout;
            openmpt_module_ctl_set(v->s.mod, "play.at_end",
                v->fadeout ? "fadeout" : "stop");
            break;
        case XMODP_KEY_AMIGA_EMU:
            v->emulate_amiga = !v->emulate_amiga;
            openmpt_module_ctl_set(v->s.mod, "render.resampler.emulate_amiga",
                v->emulate_amiga ? "1" : "0");
            break;
        case XMODP_KEY_TEMPO_UP:
            if (!(v->tempo_factor >= ftoi(2, 0, XMODP_CTL_PREC))) {
                v->tempo_factor += ftoi(0, 1, XMODP_CTL_PREC);
                openmpt_module_ctl_set(v->s.mod, "play.tempo_factor",
                        itofs(ctl_s, 64, v->tempo_factor, XMODP_CTL_PREC));
            }
            break;
        case XMODP_KEY_TEMPO_DOWN:
            if (!(v->tempo_factor <= 0)) {
                v->tempo_factor -= ftoi(0, 1, XMODP_CTL_PREC);
                openmpt_module_ctl_set(v->s.mod, "play.tempo_factor",
                        itofs(ctl_s, 64, v->tempo_factor, XMODP_CTL_PREC));
            }
            break;
        case XMODP_KEY_PITCH_UP:
            if (!(v->pitch_factor >= ftoi(2, 0, XMODP_CTL_PREC))) {
                v->pitch_factor += ftoi(0, 1, XMODP_CTL_PREC);
                openmpt_module_ctl_set(v->s.mod, "play.pitch_factor",
                        itofs(ctl_s, 64, v->pitch_factor, XMODP_CTL_PREC));
            }
            break;
        case XMODP_KEY_PITCH_DOWN:
            if (!(v->pitch_factor <= 0)) {
                v->pitch_factor -= ftoi(0, 1, XMODP_CTL_PREC);
                openmpt_module_ctl_set(v->s.mod, "play.pitch_factor",
                        itofs(ctl_s, 64, v->pitch_factor, XMODP_CTL_PREC));
            }
            break;
        case XMODP_KEY_OPLVOL_UP:
            if (!(v->oplvol_factor >= ftoi(2, 0, XMODP_CTL_PREC))) {
                v->oplvol_factor += ftoi(0, 1, XMODP_CTL_PREC);
                openmpt_module_ctl_set(v->s.mod, "render.opl.volume_factor",
                        itofs(ctl_s, 64, v->oplvol_factor, XMODP_CTL_PREC));
            }
            break;
        case XMODP_KEY_OPLVOL_DOWN:
            if (!(v->oplvol_factor <= 0)) {
                v->oplvol_factor -= ftoi(0, 1, XMODP_CTL_PREC);
                openmpt_module_ctl_set(v->s.mod, "render.opl.volume_factor",
                        itofs(ctl_s, 64, v->oplvol_factor, XMODP_CTL_PREC));
            }
            break;
        default:
            break;
    }
    return false;
}

