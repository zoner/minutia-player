#include <stdio.h>
#include "xmodp_portaudio.h"
#include "xmodp_pulseaudio.h"
#include "xmodp_globals.h"
#include "xmodp_util.h"
#include "xmodp_sound.h"

int init_sound(audio_engine e, int samplerate, audio_engine_vars *v)
{
    switch(e) {
        case AUDIO_ENGINE_PULSEAUDIO:
            v->engine_name = "PulseAudio";
            v->pulse.samplerate = samplerate;
            if (init_pulseaudio(&(v->pulse))) {
                printf("Error initializing pulseaudio.\n");
                exit(1);
            }
            break;
        case AUDIO_ENGINE_PORTAUDIO:
            v->engine_name = "PortAudio";
            v->pa.samplerate = samplerate;
            if (init_portaudio(&(v->pa))) {
                printf("Error initializing portaudio.\n");
                exit(1);
            }
            break;
        default:
            break;
    };
    return 0;
}

int push_sound(audio_engine e, audio_engine_vars *v, xmodp_buffer *buf)
{
    switch (e) {
        case AUDIO_ENGINE_PULSEAUDIO:
            pa_simple_write(
                    v->pulse.stream,
                    buf->stereo,
                    buf->filled_amount * 2 * sizeof(float),
                    NULL
            );
            break;
        case AUDIO_ENGINE_PORTAUDIO:
            v->pa.pa_error = Pa_WriteStream(
                    v->pa.stream,
                    buf->stereo, /* XXX: Untested, this could be wrong */
                    (unsigned long)(buf->filled_amount * 2 * sizeof(float)));
            break;
        default:
            break;
    };
    return 0;
}

int cleanup_sound(audio_engine e, audio_engine_vars *v)
{
    switch (e) {
        case AUDIO_ENGINE_PULSEAUDIO:
            cleanup_pulseaudio(&(v->pulse));
            break;
        case AUDIO_ENGINE_PORTAUDIO:
            break;
        default:
            break;
    };
    return 0;
}

audio_engine set_audio_engine(char *s)
{
    if (s == NULL) {
        return XMODP_DEFAULT_AUDIO_ENGINE_NUM;
    }
    if (!strcmp(s, "portaudio")) {
        return AUDIO_ENGINE_PORTAUDIO;
    }
    if (!strcmp(s, "pulse")) {
        return AUDIO_ENGINE_PULSEAUDIO;
    }
    return XMODP_DEFAULT_AUDIO_ENGINE_NUM;
}

