#ifndef XMODP_PAINT_H
#define XMODP_PAINT_H

#include "xmodp_globals.h"

void paint_refresh_main(xmodp_state *v, bool box);
void paint_title(xmodp_state *v);
void paint_shell(xmodp_state *v);
void paint_view_vu(xmodp_state *v);
void paint_view_help(xmodp_state *v);
void paint_view_samples(xmodp_state *v);
void paint_view_instruments(xmodp_state *v);
void paint_view_message(xmodp_state *v);
void paint_view_metainfo(xmodp_state *v);
void paint_view_metainfo2(xmodp_state *v);
void paint_view_mini(xmodp_state *v);
void paint_view_pattern(xmodp_state *v);
void paint_view_analyzer(xmodp_state *v);

#endif

