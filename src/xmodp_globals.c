#include "xmodp_globals.h"
#include "xmodp_openmpt.h"
#include "xmodp_paint.h"

xmodp_state v;

void set_title(xmodp_state *v, char *extra_text)
{
    if (extra_text == NULL) {
        sprintf(v->title_text, "%s", TITLE_HEADER);
    }
    else {
        sprintf(v->title_text, "%s - %s", TITLE_HEADER, extra_text);
    }
}

void set_view_title(xmodp_state *v)
{
    char *extra_text;

    switch(v->main_view) {
        case XMODP_VIEW_VU:
            extra_text = TITLE_VU;
            break;
        case XMODP_VIEW_HELP:
            extra_text = TITLE_HELP;
            break;
        case XMODP_VIEW_SAMPLES:
            extra_text = TITLE_SAMPLES;
            break;
        case XMODP_VIEW_INSTRUMENTS:
            extra_text = TITLE_INSTRUMENTS;
            break;
        case XMODP_VIEW_MESSAGE:
            extra_text = TITLE_MESSAGE;
            break;
        case XMODP_VIEW_PATTERN:
            extra_text = TITLE_TRACKS;
            break;
        case XMODP_VIEW_ANALYZER:
            extra_text = TITLE_ANALYZER;
            break;
        default:
            extra_text = NULL;
            break;
    }
    set_title(v, extra_text);
}

void handle_spectrum_analyzer(xmodp_state *v)
{
    compute_spect(&v->a, SA_SAMPLE_SZ, v->buf.mono);
}

void handle_sound(xmodp_state *v)
{
    size_t count;

    if (v->updating)
        return;
    count = xmodp_openmpt_read(&v->s, &v->buf, v->samplerate);
    if (count) {
        v->buf.filled_amount = count;
        push_sound(v->audio_engine_used, &v->audio_vars, &v->buf);
    }
    if (!count) {
        /* song has ended, do something */
        v->song_ended = true;
    }
}

bool update_screen_size(xmodp_state *v)
{
    if (check_screen_size(&v->scr, true)) {
        v->scr.main_need_refresh = true;
        v->scr.show_meta = true;
        if (v->scr.sy < META_HEIGHT + 1) {
            v->scr.show_meta = false;
        }
        v->scr.show_title = true;
        v->scr.mini_mode = false;
        if (v->scr.sy < META_HEIGHT + 4) {
            v->scr.show_title = false;
            v->scr.mini_mode = true;
        }
        if (v->scr.show_meta) {
            v->scr.main_pos_y = 4;
            v->scr.main_pos_x = 1;
            v->scr.main_width = v->scr.sx - ((v->scr.main_pos_x + 1) * 2);
            v->meta.pos_x = 4;
            v->meta.pos_y = v->scr.sy - META_HEIGHT;
            v->meta.width = (v->scr.sx - (5 * 2)) / 2;
            v->meta.height = v->scr.sy - 2 - v->meta.pos_y;
            if (v->meta.pos_y - v->scr.main_pos_y < 3) {
                v->scr.main_width = 0;
            }
            if (v->meta.width < 32) {
                v->meta2.width = 0;
                v->meta.width = v->scr.sx - 9;
            }
            else {
                v->meta2.pos_y = v->meta.pos_y;
                v->meta2.height = v->meta.height;
                v->meta2.pos_x = v->meta.pos_x + v->meta.width + 2;
                v->meta2.width = v->scr.sx - 5 - v->meta2.pos_x;
            }
        }
        else {
            /* Do not show meta(s) */
            v->meta.width = 0;
            v->meta2.width = 0;
        }
        return true;
    }
    return false;
}

void update(int signum)
{
    static uint16_t redraw_delay = 0;

    if (v.updating || v.song_ended)
        return;
    if (redraw_delay > 0) {
        redraw_delay--;
        return;
    }
    v.updating = true;

    /* Determine sizes of components based on screen size */

    if (update_screen_size(&v)) {
        redraw_delay = XMODP_RESIZE_REDRAW_DELAY;
        v.updating = false;
        return;
    }

    /* miniature mode */

    if (v.scr.mini_mode) {
        paint_view_mini(&v);
        v.updating = false;
        return;
    }

    if (v.scr.main_need_refresh) {
        paint_refresh_main(&v, (v.scr.main_width != 0) ? true : false);
        if (v.scr.show_title) {
            paint_title(&v);
        }
        paint_shell(&v);
    }

    /* paint metainfo */

    paint_view_metainfo(&v);
    paint_view_metainfo2(&v);
    /* paint main */

    if (v.scr.main_width) {
        switch(v.main_view) {
            case XMODP_VIEW_HELP:
                paint_view_help(&v);
                break;
            case XMODP_VIEW_SAMPLES:
                paint_view_samples(&v);
                break;
            case XMODP_VIEW_INSTRUMENTS:
                paint_view_instruments(&v);
                break;
            case XMODP_VIEW_MESSAGE:
                paint_view_message(&v);
                break;
            case XMODP_VIEW_VU:
                paint_view_vu(&v);
                break;
            case XMODP_VIEW_PATTERN:
                paint_view_pattern(&v);
                break;
            case XMODP_VIEW_ANALYZER:
#if USE_SPECTRUM_ANALYZER
                paint_view_analyzer(&v);
#endif
                break;
            default:
                break;
        };
    }

    v.updating = false;
}

void set_library_versions(xmodp_state *v)
{
    v->lib_meta.libopenmpt_version = xmodp_libopenmpt_get_string(
        "library_version");
}

const char *help_text[] = {
    "",
    "Welcome to Minutia Player. You found the help section.",
    "",
    "---------------| GLOBALS |---------------------------------------",
    "",
    "",
    "  q ------------ Quit",
    "  p or space --- Pause",
    "  m ------------ Mute",
    "  - / + -------- Volume down / up",
    "  ( / ) -------- OPL3 volume down / up",
    "  , / . -------- Order down / up",
    "  < / > -------- Subsong down / up",
    "  [ / ] -------- Tempo down / up",
    "  { / } -------- Pitch down / up",
    "  l ------------ Set Looping",
    "  e ------------ Set Stereo Separation",
    "  r ------------ Set Volume Ramping",
    "  y ------------ Set Filter interpolation",
    "  f ------------ Set Fadeout",
    "  a ------------ Set Amiga Paula Emulation",
    "  n ------------ Next song",
    "",
    "",
    "---------------| VIEWS |-----------------------------------------",
    "",
    "",
    "  h or ? ------- This help page",
#if 0
    "  o ------------ Options (XXX)",
#endif
    "  v ------------ VU bars",
    "  s ------------ Samples",
    "  i ------------ Instruments",
    "  g ------------ Message",
    "  t ------------ Tracks",
#if USE_SPECTRUM_ANALYZER
    "  a ------------ Spectrum analyzer (XXX)",
#endif
    "",
    "",
    "---------------| VU VIEW |---------------------------------------",
    "",
    "",
    "  w ------------ Toggle stereo bars",
    "",
    "",
    "---------------| ACKNOWLEDGMENTS |-------------------------------",
    "",
    "Minutia Player is brought to you by zoner / xylem",
    "",
    "Many thanks go out to:",
    "",
    "* Saga Musix",
    "* manx",
    "",
    "And greetings to:",
    "",
    "* nectarine",
    "",
    NULL,
};

