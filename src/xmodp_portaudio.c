#include <stdlib.h>
#include "xmodp_portaudio.h"

int init_portaudio(pa_vars *pa)
{
    pa->pa_error = Pa_Initialize();
    if (pa->pa_error != paNoError) {
        return 1;
    }
    pa->pa_error = Pa_OpenDefaultStream(
            &pa->stream,
            0,
            2,
            paFloat32 | paNonInterleaved,
            pa->samplerate,
            paFramesPerBufferUnspecified,
            NULL,
            NULL);
    if (pa->pa_error != paNoError) {
        return 1;
    }
    pa->pa_error = Pa_StartStream(pa->stream);
    return (pa->pa_error != paNoError);
}

