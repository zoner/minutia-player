#ifndef XMODP_ARG_H
#define XMODP_ARG_H

typedef struct {
    long int samplerate;
    char **files;
    char *audio_engine;
    unsigned int refresh_rate;
    unsigned int view;
    bool show_versions;
    unsigned int loop_mode;
    bool use_unicode;
    bool vu_stereo;
    unsigned int stereo_separation_percent;
    int volume_ramping_strength;
    unsigned int interpolation_filter_length;
    bool fadeout;
    int tempo_factor;
    int pitch_factor;
    bool emulate_amiga;
    int oplvol_factor;
} arguments;

void parse_arguments(int argc, char **argv, arguments *arguments);

#endif
