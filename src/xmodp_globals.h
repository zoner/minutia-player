#ifndef XMODP_GLOBALS_H
#define XMODP_GLOBALS_H

#include <stdbool.h>
#include "xmodp_openmpt.h"
#include "xmodp_screen.h"
#include "xmodp_sound.h"
#include "xmodp_portaudio.h"
#include "xmodp_pulseaudio.h"
#include "xmodp_spect.h"
#include "xmodp_const.h"

typedef struct {
    const char *xmodp_version;
    const char *libopenmpt_version;
} library_metainfo;

typedef enum {
    XMODP_VIEW_VU = 0,
    XMODP_VIEW_SAMPLES,
    XMODP_VIEW_INSTRUMENTS,
    XMODP_VIEW_MESSAGE,
    XMODP_VIEW_HELP,
    XMODP_VIEW_PATTERN,
    XMODP_VIEW_ANALYZER,
    XMODP_NUM_VIEWS,
} xmodp_view;

typedef struct {
    int pos_y;
    int pos_x;
    int width;
    int height;
    int chan_num_width;
    int effect_width;
    int sample_width;
    int note_width;
    bool stereo;
} xmodp_view_vu;

typedef struct {
    int pos_y;
    int pos_x;
    int width;
    int height;
} xmodp_view_meta;

typedef struct {
    int idx;
} xmodp_view_help;

typedef struct {
    int idx;
} xmodp_view_sample;

typedef struct {
    int idx;
} xmodp_view_instrument;

typedef struct {
    int idx;
} xmodp_view_message;

typedef struct {
    bool quit;
    bool song_ended;
    bool updating;
    library_metainfo lib_meta;
    xmodp_openmpt_song s;           /* song and song meta */
    xmodp_screen scr;               /* screen state */
    xmodp_spectrum_analyzer_t a;    /* spectrum analyzer */
    char title_text[1024];

    /* views */

    xmodp_view_vu vu;                   /* VU state */
    xmodp_view_meta meta;               /* metainfo state */
    xmodp_view_meta meta2;              /* second metainfo state */
    xmodp_view_help help;               /* help state */
    xmodp_view_sample sample;           /* sampleview state */
    xmodp_view_instrument instrument;   /* instrument view state */
    xmodp_view_message message;         /* message view state */

    /* state vars */

    xmodp_buffer buf;
    audio_engine_vars audio_vars;
    audio_engine audio_engine_used;
    int samplerate;                         /* sampling rate */
    int repeat;                             /* update speed in milliseconds */
    int32_t master_gain_millibel;           /* default 0 */
    int32_t stereo_separation_percent;      /* 0-200, default 100 */
    int32_t volume_ramping_strength;        /* -1 - 10, def. -1 recommended */
    int32_t interpolation_filter_length;
    /* interpolation filter length:
        0: default
        1: none
        2: linear
        4: cubic
        8: windowed sinc with 8 taps
    */
    int loop;
    bool muted;
    bool paused;
    bool unicode;
    bool fadeout;
    int tempo_factor;
    int pitch_factor;
    bool emulate_amiga;
    int oplvol_factor;

    xmodp_view main_view;
} xmodp_state;

//xmodp_state v; /* state variables */

void set_title(xmodp_state *v, char *extra_text);
void set_view_title(xmodp_state *v);
void handle_spectrum_analyzer(xmodp_state *v);
void update(int signum);
void set_library_versions(xmodp_state *v);
void handle_sound(xmodp_state *v);

extern const char *help_text[];

#endif

