#ifndef XMODP_SOUND_H
#define XMODP_SOUND_H

#include "xmodp_portaudio.h"
#include "xmodp_pulseaudio.h"

typedef struct {
    pa_vars pa;
    pulse_vars pulse;
    const char *engine_name;
} audio_engine_vars;

typedef enum {
    AUDIO_ENGINE_PULSEAUDIO,
    AUDIO_ENGINE_PORTAUDIO
} audio_engine;

int init_sound(audio_engine e, int samplerate, audio_engine_vars *v);
int push_sound(audio_engine e, audio_engine_vars *v, xmodp_buffer *buf);
int cleanup_sound(audio_engine e, audio_engine_vars *v);
audio_engine set_audio_engine(char *s);

#endif

