#include <ncurses.h>
#include "xmodp_globals.h"
#include "xmodp_tk.h"
#include "xmodp_paint.h"

void paint_title(xmodp_state *v)
{
    if (v->scr.sx > 16) {
        paint_box(1, 4, 3, v->scr.sx - 5,
            XMODP_COL_BOX_HIGH, XMODP_COL_BOX_LOW);
        paint_blank(1, v->scr.sx - 4, 4, v->scr.sx - 1, NULL);
        xmodp_set_color(XMODP_COL_MAIN);
        paint_blank(2, 5, 3, v->scr.sx - 5, v->title_text);
    }
}

void paint_refresh_main(xmodp_state *v, bool box)
{
    v->scr.main_need_refresh = false;
    xmodp_set_color(XMODP_COL_MAIN);
    paint_blank(1, 1, v->scr.sy - 1 , v->scr.sx - 1, NULL);

    /* Box around main view */
    if (box) {
        paint_box(
                v->scr.main_pos_y,
                v->scr.main_pos_x,
                v->meta.pos_y - 1,
                v->scr.sx - 2,
                XMODP_COL_BOX_LOW,
                XMODP_COL_BOX_HIGH);
    }
}

void paint_shell(xmodp_state *v)
{
    /* Box around entire screen */

    if (v->scr.sy > 4 && v->scr.sx > 4) {
        paint_box(0, 0, v->scr.sy - 1, v->scr.sx - 1,
                XMODP_COL_BOX_HIGH, XMODP_COL_BOX_LOW);
    }
}

void print_help_line(int y, int x, const char *text, size_t maxlen)
{
    size_t i = 0;
    char *t;
    size_t len;

    len = strlen(text);
    if (len > maxlen) {
        len = maxlen;
    }
    t = (char*)malloc(maxlen + 1);
    snprintf(t, len + 1, text);
    t[len] = '\0';

    switch (t[0]) {
        case '-':
            move(y, x);
            while (t[i] != '\0') {
                if (t[i] == '-') {
                    xmodp_set_color(XMODP_COL_MAIN);
                }
                else {
                    xmodp_set_color(XMODP_COL_TRACK_NOTE);
                }
                addch(t[i++]);
            }
            break;
        case ' ':
            move(y, x);
            xmodp_set_color(XMODP_COL_VU_LOW);
            while (i < 3 || t[i] != '-') {
                if (t[i] == '/') {
                    xmodp_set_color(XMODP_COL_BOX_LOW);
                }
                addch(t[i++]);
                xmodp_set_color(XMODP_COL_VU_LOW);

            }
            xmodp_set_color(XMODP_COL_BOX_LOW);
            while (t[i] == '-') {
                addch(t[i++]);
            }
            xmodp_set_color(XMODP_COL_MAIN);
            printw(t + i);
            break;
        default:
            xmodp_set_color(XMODP_COL_VU_INSTRUMENT);
            mvprintw(y, x, t);
            break;
    };
    free(t);
}

void paint_view_help(xmodp_state *v)
{
    int i;
    int height;

    height = v->meta.pos_y - v->scr.main_pos_y - 2;
    for (i = 0; i < height; i++) {
        int idx = i + v->help.idx;
        if (help_text[idx] == NULL) {
            break;
        }
        print_help_line(
                v->scr.main_pos_y + i + 1,
                v->scr.main_pos_x + 2,
                help_text[idx],
                v->scr.main_width - 2);
    }
}

void paint_view_vu(xmodp_state *v)
{
    int i;
    size_t max_sample_width;
    size_t min_vu_width;
    bool paint_notes;
    bool paint_instruments;
    bool paint_effects;
    bool paint_stereo;
    bool paint_chan_nums;

    paint_chan_nums = true;
    paint_notes = true;
    paint_instruments = true;
    paint_effects = true;
    paint_stereo = true;
    max_sample_width = 31; /* Looks like this is the max in openmpt */
    min_vu_width = 8;

    /* XXX to ignore set but not used error */
    if (paint_notes && paint_instruments && paint_effects && paint_stereo) {
        paint_stereo = false;
    }

    v->vu.pos_y = v->scr.main_pos_y + 1;
    v->vu.pos_x = v->scr.main_pos_x + 1;
    v->vu.chan_num_width = 2 + 1; /* TODO: determine from mod's num channels */
    v->vu.effect_width = 8 + 1;
    v->vu.sample_width = max_sample_width + 1;
    v->vu.note_width = 3 + 1;
    v->vu.width = v->scr.main_width;

    /* Degrade gracefully */

    if (v->scr.main_width <
            v->vu.chan_num_width + v->vu.note_width + v->vu.sample_width +
            v->vu.effect_width + min_vu_width) {
        paint_effects = false;
    }
    if (v->scr.main_width <
            v->vu.chan_num_width + v->vu.note_width +
            v->vu.sample_width + min_vu_width) {
        paint_instruments = false;
    }
    if (paint_chan_nums) {
        v->vu.width -= v->vu.chan_num_width;
        v->vu.pos_x += v->vu.chan_num_width;
    }
    if (paint_notes) {
        v->vu.width -= v->vu.note_width;
        v->vu.pos_x += v->vu.note_width;
    }
    if (paint_instruments) {
        v->vu.width -= v->vu.sample_width;
        v->vu.pos_x += v->vu.sample_width;
    }
    if (paint_effects) {
        v->vu.width -= v->vu.effect_width;
    }
    v->vu.height = v->s.num_channels;

    for (i = 0; i < v->s.num_channels; i++) {
        if (v->vu.pos_y + i > v->meta.pos_y - 2) {
            break;
        }

        move(v->vu.pos_y + i, v->scr.main_pos_x + 1);
        if (paint_chan_nums) {
            xmodp_set_color(XMODP_COL_VU_CHANNEL_NUM);
            printw("%02i ", i + 1);
        }
        if (paint_notes) {
            const char *note_str;
            xmodp_set_color(XMODP_COL_VU_NOTE);
            note_str = "   ";
            if (v->s.channel[i].vu_mono) {
                note_str = v->s.channel[i].note;
            }
            if (note_str[0] == '.') {
                note_str = "   ";
            }
            printw(note_str);
            printw(" ");
        }
        if (paint_instruments) {
            const char *sample_text;
            size_t t_len;
            size_t padding;
            int inst_idx;

            xmodp_set_color(XMODP_COL_VU_INSTRUMENT);
            /* determine sample number played at current pattern/row/track */
            inst_idx = v->s.channel[i].instrument_idx;
            sample_text = "";
            if (v->s.channel[i].vu_mono) {
                if (inst_idx > 0) {
                    if (v->s.num_instruments &&
                            inst_idx <= v->s.num_instruments) {
                        sample_text = v->s.instrument[inst_idx - 1].name;
                    }
                    else if (inst_idx <= v->s.num_samples) {
                        sample_text = v->s.sample[inst_idx -1].name;
                    }
                }
            }
            t_len = strlen(sample_text);
            padding = 0;
            if (t_len < max_sample_width) {
                padding = max_sample_width - t_len;
            }
            printw(sample_text);
            for (t_len = 0; t_len < padding; t_len++) {
                addch(' ');
            }
            addch(' ');
        }
        if (v->vu.stereo) {
            paint_vu_stereo_horiz(
                    v->vu.pos_y + i,
                    v->vu.pos_x,
                    v->vu.width,
                    v->s.channel[i].vu_left,
                    v->s.channel[i].vu_right,
                    v->unicode,
                    XMODP_COL_VU_BG,
                    XMODP_COL_VU_LOW,
                    XMODP_COL_VU_MED,
                    XMODP_COL_VU_HIGH);
        }
        else {
            paint_vu_horiz(
                    v->vu.pos_y + i,
                    v->vu.pos_x,
                    v->vu.width,
                    v->s.channel[i].vu_mono,
                    v->unicode,
                    XMODP_COL_VU_BG,
                    XMODP_COL_VU_LOW,
                    XMODP_COL_VU_MED,
                    XMODP_COL_VU_HIGH);
        }
        if (paint_effects) {
            xmodp_set_color(XMODP_COL_VU_EFFECT);
            mvprintw(
                    v->vu.pos_y + i,
                    v->vu.pos_x + v->vu.width + 1,
                    "        ");
            mvprintw(
                    v->vu.pos_y + i,
                    v->vu.pos_x + v->vu.width + 1,
                    human_effect(
                            &v->s,
                            v->s.channel[i].effect,
                            v->s.channel[i].effect_value));
        }
    }
}

void paint_main_message(xmodp_state *v, char *message)
{
    xmodp_set_color(XMODP_COL_INFO);
    paint_blank(
            v->scr.main_pos_y + 1,
            v->scr.main_pos_x + 1,
            v->meta.pos_y - 1,
            v->scr.sx - 2,
            message);

}

void paint_view_samples(xmodp_state *v)
{
    int i;
    int height;

    if (v->s.num_samples == 0) {
        paint_main_message(v, "no samples");
        return;
    }
    height = v->meta.pos_y - v->scr.main_pos_y - 2;

    if (v->s.num_samples  < height) {
        height = v->s.num_samples - v->sample.idx;
    }
    for (i = 0; i < height; i++) {
        int idx = i + v->sample.idx;
        xmodp_set_color(XMODP_COL_BOLD);
        mvprintw(
                v->scr.main_pos_y + i + 1,
                v->scr.main_pos_x + 1,
                "%02i: ",
                idx);
        xmodp_set_color(XMODP_COL_SAMPLE);
        printw(v->s.sample[idx].name);
    }
}

void paint_view_instruments(xmodp_state *v)
{
    int i;
    int height;

    if (v->s.num_instruments == 0) {
        paint_main_message(v, "no instruments");
        return;
    }
    height = v->meta.pos_y - v->scr.main_pos_y - 2;
    if (v->s.num_instruments  < height) {
        height = v->s.num_instruments - v->instrument.idx;
    }
    for (i = 0; i < height; i++) {
        int idx = i + v->instrument.idx;
        xmodp_set_color(XMODP_COL_BOLD);
        mvprintw(
                v->scr.main_pos_y + i + 1,
                v->scr.main_pos_x + 1,
                "%02i: ",
                idx);
        xmodp_set_color(XMODP_COL_INSTRUMENT);
        printw(v->s.instrument[idx].name);
    }
}

void paint_view_message(xmodp_state *v)
{
    int i;
    int height;
    xmodp_message_line_t *cur_line;
    char *str;

    if (v->s.message.num_lines == 0) {
        paint_main_message(v, "no message");
        return;
    }

    height = v->meta.pos_y - v->scr.main_pos_y - 2;
    if (v->s.message.num_lines  < height) {
        height = v->s.message.num_lines - v->message.idx;
    }
    cur_line = v->s.message.line_list;
    for (i = 0; i < v->message.idx; i++) {
        cur_line = cur_line->next;
    }
    for (i = 0; i < height; i++) {
        int idx = i + v->message.idx;
        xmodp_set_color(XMODP_COL_BOLD);
        mvprintw(
                v->scr.main_pos_y + i + 1,
                v->scr.main_pos_x + 1,
                "%02i: ",
                idx);
        xmodp_set_color(XMODP_COL_MESSAGE);
        str = copy_str(cur_line->text, (v->scr.sx - 3) - (v->scr.main_pos_x + 2 + 4));
        printw(str);
        free(str);
        cur_line = cur_line->next;
    }
}

void paint_view_metainfo(xmodp_state *v)
{
    int i;
    int x;
    int max_field_len;
    int max_text_len;
    char line[MAX_META_LINE_LEN + 2];

    if (!v->meta.width) {
        return;
    }

    max_text_len = v->meta.width - 2 - 1;
    if (max_text_len > MAX_META_LINE_LEN) {
        max_text_len = MAX_META_LINE_LEN;
    }
    if (max_text_len < 0) {
        max_text_len = 0;
    }
    max_field_len = max_text_len - 15;
    if (max_field_len < 0) {
        max_field_len = 0;
    }

    i = v->meta.pos_y + 1;
    x = v->meta.pos_x + 2;
    xmodp_set_color(XMODP_COL_MAIN);
    paint_blank(
            v->meta.pos_y,
            v->meta.pos_x,
            v->meta.pos_y + v->meta.height,
            v->meta.pos_x + v->meta.width,
            NULL);
    if (max_text_len) {
        move(i++, x);
        char s[1024];
        if (v->s.artist != NULL && v->s.artist[0] != '\0') {
            snprintf(s, 1023, "%s * %s", v->s.artist, v->s.title);
        }
        else {
            snprintf(s, 1023, "%s", v->s.title);
        }
        x_printw(s, max_text_len);
    }
    if (max_field_len) {
        xmodp_set_color(XMODP_COL_BOLD);
        mvprintw(i++, x, "Module Type  : ");
        xmodp_set_color(XMODP_COL_MAIN);
        line[max_field_len - 1] = '\0';
        if (v->s.tracker[0] == '\0') {
            snprintf(line, max_field_len + 1, "%s", v->s.type_long);
        }
        else {
            snprintf(line, max_field_len + 1,
                    "%s (%s)", v->s.type_long, v->s.tracker);
        }
        if (line[max_field_len - 1] != '\0') {
            line[max_field_len - 1] = '\0';
            snprintf(line, max_field_len + 1, v->s.type_long);
        }
        if (line[max_field_len - 1] != '\0') {
            snprintf(line, max_field_len + 1, v->s.type);
        }
        printw(line);
        xmodp_set_color(XMODP_COL_BOLD);
        mvprintw(i++, x, "Channels     : ");
        xmodp_set_color(XMODP_COL_MAIN);
        snprintf(line, max_field_len + 1,
                "%i (%i)", v->s.num_channels, v->s.current_play_channels);
        printw(line);
        xmodp_set_color(XMODP_COL_BOLD);
        mvprintw(i++, x, "Order        : ");
        xmodp_set_color(XMODP_COL_MAIN);
        snprintf(line, max_field_len + 1,
                "%02i/%02i", v->s.order + 1, v->s.num_orders);
        printw(line);
        xmodp_set_color(XMODP_COL_BOLD);
        mvprintw(i++, x, "Row          : ");
        xmodp_set_color(XMODP_COL_MAIN);
        snprintf(line, max_field_len + 1,
                "%02i/%02i", v->s.row + 1, v->s.num_rows);
        printw(line);
        xmodp_set_color(XMODP_COL_BOLD);
        mvprintw(i++, x, "Pattern      : ");
        xmodp_set_color(XMODP_COL_MAIN);
        snprintf(line, max_field_len + 1, "%02i", v->s.pattern);
        printw(line);
        xmodp_set_color(XMODP_COL_BOLD);
        mvprintw(i++, x, "Speed/Tempo  : ");
        xmodp_set_color(XMODP_COL_MAIN);
        snprintf(line, max_field_len + 1, "%02i/%03i", v->s.speed, v->s.tempo);
        printw(line);
        xmodp_set_color(XMODP_COL_BOLD);
        mvprintw(i++, x, "Time         : ");
        xmodp_set_color(XMODP_COL_MAIN);
        snprintf(line, max_field_len + 1,
                "%lu:%02lu / %lu:%02lu",
                (uint64_t)(v->s.current_pos_seconds) / 60,
                (uint64_t)(v->s.current_pos_seconds) % 60,
                (uint64_t)(v->s.duration_seconds) / 60,
                (uint64_t)(v->s.duration_seconds) % 60
        );
        printw(line);
        xmodp_set_color(XMODP_COL_BOLD);
        mvprintw(i++, x, "Subsong      : ");
        xmodp_set_color(XMODP_COL_MAIN);
        snprintf(line, max_field_len + 1,
                "%i/%i", v->s.current_subsong + 1, v->s.num_subsongs);
        printw(line);
    }
    paint_box(
            v->meta.pos_y,
            v->meta.pos_x,
            v->meta.pos_y + v->meta.height,
            v->meta.pos_x + v->meta.width,
            XMODP_COL_BOX_LOW,
            XMODP_COL_BOX_HIGH);
}

void paint_view_metainfo2(xmodp_state *v) {
    int i;
    int x;
    const char *interp;
    char fstr[64];

    if (!v->meta2.width) {
        return;
    }
    xmodp_set_color(XMODP_COL_MAIN);
    paint_blank(
            v->meta2.pos_y,
            v->meta2.pos_x,
            v->meta2.pos_y + v->meta2.height,
            v->meta2.pos_x + v->meta2.width,
            NULL);
    i = v->meta2.pos_y + 1;
    x = v->meta2.pos_x + 2;
    xmodp_set_color(XMODP_COL_MAIN);
    mvprintw(i++, x, "%s (%ikHz 32 bit)",
            v->audio_vars.engine_name, v->samplerate / 1000);
    xmodp_set_color(XMODP_COL_BOLD);
    mvprintw(i, x, "Loop Mode     : ");
    xmodp_set_color(XMODP_COL_MAIN);
    switch (v->loop) {
        case -1:
            printw("Inf");
            break;
        case 0:
            printw("None");
            break;
        default:
            printw("%i", v->loop);
            break;
    }
    xmodp_set_color(XMODP_COL_KEYINFO);
    mvprintw(i++, x + v->meta2.width - 6, "[l]");
    xmodp_set_color(XMODP_COL_BOLD);
    mvprintw(i, x, "Stereo Sep.   : ");
    xmodp_set_color(XMODP_COL_MAIN);
    printw("%3i%%", v->stereo_separation_percent);
    xmodp_set_color(XMODP_COL_KEYINFO);
    mvprintw(i++, x + v->meta2.width - 6, "[e]");
    xmodp_set_color(XMODP_COL_BOLD);
    mvprintw(i, x, "Vol Ramping   : ");
    xmodp_set_color(XMODP_COL_MAIN);
    switch (v->volume_ramping_strength) {
        case -1:
            printw("Default");
            break;
        case 0:
            printw("None");
            break;
        default:
            printw("%i", v->volume_ramping_strength);
            break;
    }
    xmodp_set_color(XMODP_COL_KEYINFO);
    mvprintw(i++, x + v->meta2.width - 6, "[r]");
    xmodp_set_color(XMODP_COL_BOLD);
    mvprintw(i, x, "Interpolation : ");
    xmodp_set_color(XMODP_COL_MAIN);
    switch (v->interpolation_filter_length) {
        case 0:
            interp = "Default";
            break;
        case 1:
            interp = "None";
            break;
        case 2:
            interp = "Linear";
            break;
        case 4:
            interp = "Cubic";
            break;
        case 8:
            interp = "Windowed";
            break;
        default:
            interp = "Unknown";
    }
    printw("%s", interp);
    xmodp_set_color(XMODP_COL_KEYINFO);
    mvprintw(i++, x + v->meta2.width - 6, "[y]");
    xmodp_set_color(XMODP_COL_BOLD);
    mvprintw(i, x, "Tempo / Pitch : ");
    xmodp_set_color(XMODP_COL_MAIN);
    printw("%s", itofs(fstr, 63, v->tempo_factor, XMODP_CTL_PREC));
    xmodp_set_color(XMODP_COL_BOLD);
    printw(" / ");
    xmodp_set_color(XMODP_COL_MAIN);
    printw("%s", itofs(fstr, 63, v->pitch_factor, XMODP_CTL_PREC));
    xmodp_set_color(XMODP_COL_KEYINFO);
    mvprintw(i++, x + v->meta2.width - 8, "[[,{]");
    xmodp_set_color(XMODP_COL_BOLD);
    mvprintw(i, x, "OPL volume    : ");
    xmodp_set_color(XMODP_COL_MAIN);
    printw("%s", itofs(fstr, 63, v->oplvol_factor, XMODP_CTL_PREC));
    xmodp_set_color(XMODP_COL_KEYINFO);
    mvprintw(i++, x + v->meta2.width - 8, "[(/)]");
    xmodp_set_color(XMODP_COL_BOLD);
    mvprintw(i, x, "Master Gain   : ");
    xmodp_set_color(XMODP_COL_MAIN);
    if (v->muted) {
        xmodp_set_color(XMODP_COL_KEYINFO);
    }
    printw("%i", v->master_gain_millibel);
    xmodp_set_color(XMODP_COL_KEYINFO);
    mvprintw(i++, x + v->meta2.width - 8, "[-/+]");
    xmodp_set_color(XMODP_COL_BOLD);
    mvprintw(i, x, "Fadeout (");
    xmodp_set_color(XMODP_COL_MAIN);
    print_check(v->unicode, v->fadeout);
    xmodp_set_color(XMODP_COL_KEYINFO);
    xmodp_set_color(XMODP_COL_BOLD);
    printw(")  ");
    if (!v->s.is_amiga) {
        xmodp_set_color(XMODP_COL_KEYINFO);
    }
    printw("Amiga");
    xmodp_set_color(XMODP_COL_BOLD);
    printw(" (");
    xmodp_set_color(XMODP_COL_MAIN);
    print_check(v->unicode, v->emulate_amiga);
    xmodp_set_color(XMODP_COL_BOLD);
    printw(")");
    xmodp_set_color(XMODP_COL_KEYINFO);
    mvprintw(i++, x + v->meta2.width - 8, "[f,a]");

    paint_box(
            v->meta2.pos_y,
            v->meta2.pos_x,
            v->meta2.pos_y + v->meta2.height,
            v->meta2.pos_x + v->meta2.width,
            XMODP_COL_BOX_LOW,
            XMODP_COL_BOX_HIGH);
}

void paint_view_mini(xmodp_state *v)
{
    xmodp_set_color(XMODP_COL_KEYINFO);
    paint_blank(0, 0, v->scr.sy, v->scr.sx, "mini mode TK");
}

void set_pattern_column_color(int col_type, bool cur_row)
{
    if (cur_row) {
        switch (col_type) {
            case 0: /* track divider */
                xmodp_set_color(XMODP_COL_TRACK_CUR_DIVIDER);
                break;
            case 1: /* note */
                xmodp_set_color(XMODP_COL_TRACK_CUR_NOTE);
                break;
            case 2: /* instrument */
                xmodp_set_color(XMODP_COL_TRACK_CUR_INSTRUMENT);
                break;
            case 3: /* effect */
                xmodp_set_color(XMODP_COL_TRACK_CUR_EFFECT);
                break;
            default:
                xmodp_set_color(XMODP_COL_TRACK_CUR_NOTE);
        }
    }
    else {
        switch (col_type) {
            case 0: /* track divider */
                xmodp_set_color(XMODP_COL_TRACK_DIVIDER);
                break;
            case 1: /* note */
                xmodp_set_color(XMODP_COL_TRACK_NOTE);
                break;
            case 2: /* instrument */
                xmodp_set_color(XMODP_COL_TRACK_INSTRUMENT);
                break;
            case 3: /* effect */
                xmodp_set_color(XMODP_COL_TRACK_EFFECT);
                break;
            default:
                xmodp_set_color(XMODP_COL_TRACK_NOTE);
        }
    }
}

void paint_pattern_row(char *row_str, int y, int x, int width, bool cur_row)
{
    int i;
    char *p;
    int col;

    p = row_str;
    col = 1;
    move(y, x);
    set_pattern_column_color(col, cur_row);
    for (i = 0; i < width; i++) {
        switch (*p) {
            case '\0':
                addch(' ');
                continue;
            case ' ':
                col++;
                set_pattern_column_color(col, cur_row);
                addch(*p);
                break;
            case '|':
                col = 0;
                set_pattern_column_color(col, cur_row);
                addch(ACS_VLINE);
                break;
            default:
                addch(*p);
                break;
        }
        p++;
    }
}

void paint_view_pattern(xmodp_state *v)
{
    int i;
    int j;
    int height;
    int width;
    int start_idx;
    bool is_current_row;
    int current_row;
    int current_pattern;

    current_pattern = v->s.pattern;
    current_row = v->s.row;
    width = v->scr.sx - v->scr.main_pos_x - 5;
    height = v->meta.pos_y - v->scr.main_pos_y - 2;

    start_idx = current_row - (height / 2);

    for (i = 0; i < height; i++) {
        is_current_row = false;
        if (start_idx + i == current_row) {
            is_current_row = true;
        }
        set_pattern_column_color(0, is_current_row);
        if (start_idx + i <= 0) {
            /* draw empty line at the end */
            move(v->scr.main_pos_y + 1 + i, v->scr.main_pos_x + 2);
            for (j = 0; j < width; j++) {
                addch(' ');
            }
            continue;
        }
        if (start_idx + i >= v->s.num_rows) {
            /* draw empty line at the end */
            move(v->scr.main_pos_y + 1 + i, v->scr.main_pos_x + 2);
            for (j = 0; j < width; j++) {
                addch(' ');
            }
            break;
        }
        paint_pattern_row(
                v->s.pattern_data[current_pattern].row[start_idx + i].text,
                v->scr.main_pos_y + 1 + i,
                v->scr.main_pos_x + 2,
                width,
                is_current_row);
    }
}

void paint_view_analyzer(xmodp_state *v)
{
    size_t i;
    size_t j;
    size_t width;
    size_t height;
    size_t num_points;
    double scaled_val;
    size_t points_per_col;

    width = v->scr.sx - v->scr.main_pos_x - 5;
    points_per_col = (SA_SAMPLE_SZ / 2) / width;
    height = v->meta.pos_y - v->scr.main_pos_y - 2;
    num_points = width;
    if (num_points > SA_SAMPLE_SZ / 2) {
        num_points = SA_SAMPLE_SZ / 2;
    }

    for (i = 0; i < width; i++) {
        double result = 0;
        for (j = 0; j < points_per_col; j++) {
            result += v->a.result[(i * points_per_col) + j];
        }
        result /= points_per_col;
        xmodp_set_color(XMODP_COL_SPECTRUM_ANALYZER);
        scaled_val = abs(result);
        //scaled_val /= 1000;
        if (scaled_val > height) {
            scaled_val = height;
            //scaled_val = 0;
        }
        for (j = v->scr.main_pos_y + 1; j < v->meta.pos_y - scaled_val - 1; j++) {
            move(j, v->scr.main_pos_x + 2 + i);
            addch(' ');
        }
        for (j = v->meta.pos_y - scaled_val; j < v->meta.pos_y - 1; j++) {
            move(j, v->scr.main_pos_x + 2 + i);
            addch(ACS_CKBOARD);
        }
        if (scaled_val > 2) {
            move(v->meta.pos_y - scaled_val - 1, v->scr.main_pos_x + 2 + i);
            xmodp_set_color(XMODP_COL_SPECTRUM_ANALYZER_PEAK);
            //addch(ACS_CKBOARD);
            //addch(ACS_S9);
            addch('_');
        }
    }
    //v->scr.main_need_refresh = true;

}

