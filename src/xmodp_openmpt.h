#ifndef XMODP_OPENMPT_H
#define XMODP_OPENMPT_H

#include <stdbool.h>
#include <libopenmpt/libopenmpt.h>
#include <libopenmpt/libopenmpt_stream_callbacks_file.h>
#include "xmodp_util.h"

#define MAX_CHANNELS (128)
#define MAX_SAMPLES (256)
#define MAX_INSTRUMENTS (256)

#define XMODP_DECLARE_FORMAT(a, b, c, d) XMODP_FORMAT_##a,
typedef enum {
    XMODP_FORMAT_UNKNOWN = 0,
    #include "xmodp_openmpt_formats.h"
    XMODP_NUM_FORMATS,
} xmodp_format;
#undef XMODP_DECLARE_FORMAT

typedef struct {
    float vu_mono;
    float vu_left;
    float vu_right;
    const char *effect;
    const char *effect_value;
    const char *note;
    int instrument_idx;
} xmodp_channel_t;

typedef struct {
    const char *name;

} xmodp_sample_t;

typedef struct {
    const char *name;
} xmodp_instrument_t;

typedef struct xmodp_message_line_t {
    struct xmodp_message_line_t *next;
    char *text;
} xmodp_message_line_t;

typedef struct {
    char *message;
    xmodp_message_line_t *line_list;
    size_t num_lines;
} xmodp_message_t;

typedef struct xmodp_row_t {
    char *text;
} xmodp_row_t;

typedef struct {
    xmodp_row_t *row;
    uint16_t num_rows;
} xmodp_pattern_t;

typedef struct xmodp_effect_field_t {
    int effect;
    struct xmodp_effect_field_t *extension;
} xmodp_effect_field_t;

#define XMODP_DECLARE_CTL(x, y) XMODP_MPT_CTL_##x,
typedef enum xmodp_openmpt_ctl {
    XMODP_MPT_CTL_NONE=0,
    #include "xmodp_openmpt_ctls.h"
    XMODP_MPT_CTL_COUNT
} xmodp_openmpt_ctl;
#undef XMODP_DECLARE_CTL

typedef struct {
    char *filename;
    FILE *file;
    openmpt_module *mod;

    /* Static metadata */

    xmodp_format format;
    bool is_amiga;
    xmodp_effect_field_t effect_names[256];
    const char *title;
    const char *type;
    const char *type_long;
    const char *originaltype;
    const char *originaltype_long;
    const char *container;
    const char *container_long;
    const char *tracker;
    const char *artist;
    const char *message_raw;

    int32_t num_samples;
    int32_t num_instruments;
    int32_t num_orders;
    int32_t num_channels;
    double duration_seconds;
    int32_t current_subsong;
    int32_t num_subsongs;

    /* Dynamic metadata */

    int32_t order;
    int32_t num_rows;
    int32_t row;
    int32_t pattern;
    int32_t speed;
    int32_t tempo;
    int32_t current_play_channels;
    double current_pos_seconds;
    xmodp_channel_t channel[MAX_CHANNELS];
    xmodp_sample_t sample[MAX_SAMPLES];
    xmodp_instrument_t instrument[MAX_INSTRUMENTS];
    xmodp_message_t message;
    xmodp_pattern_t *pattern_data;
} xmodp_openmpt_song;

int xmodp_openmpt_init(
        xmodp_openmpt_song *s, int32_t master_gain_millibel, int loop,
        int32_t stereo_separation_percent, int32_t volume_ramping_strength,
        int32_t interpolation_filter_length, bool fadeout, char *tempo_factor,
        char *pitch_factor, bool emulate_amiga, char *oplvol_factor,
        bool muted);
void xmodp_openmpt_update(xmodp_openmpt_song *s);
size_t xmodp_openmpt_read(xmodp_openmpt_song *s, xmodp_buffer *buf, int rate);
const char * human_effect(
        xmodp_openmpt_song *s, const char *effect, const char *value);
const char *xmodp_libopenmpt_get_string(const char *s);
void xmodp_openmpt_destroy(xmodp_openmpt_song *s);

#endif

