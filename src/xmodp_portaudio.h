#ifndef XMODP_PORTAUDIO_H
#define XMODP_PORTAUDIO_H

#include <stdbool.h>
#include <portaudio.h>

typedef struct {
    int samplerate;
    PaStream *stream;
    bool pa_initialized;
    PaError pa_error;
} pa_vars;

int init_portaudio(pa_vars *pa);

#endif

